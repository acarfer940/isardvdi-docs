# Booking a desktop

Some resources in the system have to be shared among users, like GPU. This bookings icon will let you make a new future booking to use this desktop.

In the calendar you will see when the actual *GPU profile* set in the virtual desktop has availability in the system. 


You can add your booking with drag and drop within the limits your user has:
