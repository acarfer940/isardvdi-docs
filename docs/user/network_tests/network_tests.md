# Testing with networks

**Networks** in IsardVDI give rise to a lot of **different scenarios** for **each kind of practice**.

In this manual, **Different network types** are explained that can be configured on virtual desktops and **use case examples** for each type.

First **figure** all networks on a desktop and as configured **denote** their **function** and **use cases** are set.


## User Manual

### Network assignment on desktop

1. Creating a desktop based on a template, or on an already created desktop [modifies the Networks section in Hardware](https://isard.gitlab.io/isardvdi-docs/user/edit,desktop.ca/#hardware), adding **eight different networks**

![](./network_tests.images/crear_escriptori.png)

![](./network_tests.images/redes.png)

2.- The desktop is **booted** and a **terminal** opens where 8 added networks are checked, typing ```ip link show```:
```
isard:ubuntu:$$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state unknown mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00 brd 00:00:00:00:00:00:00
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff:ff
3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:35:1d brd ff:ff:ff:ff:ff:ff
4: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3a:5e:b9 brd ff:ff:ff:ff:ff:ff
5: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3b:75:c9 brd ff:ff:ff:ff:ff:ff
6: enp5s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:03:c8:83 brd ff:ff:ff:ff:ff:ff
7: enp6s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:41:76:68 brd ff:ff:ff:ff:ff:ff
8: enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:19:cb:5f brd ff:ff:ff:ff:ff:ff:ff
9: enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1366 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:de:9f brd ff:ff:ff:ff:ff:ff:ff
```

*NOTE*: The **command** of network cards that are configured on the desktop in Isard is the order of cards that appear configured on the desktop system, for example when typing the **ip -c a** command in the terminal


#### Renaming Network Cards

To **facilitate** the experience and **fasten** the user’s work, you can **modify the names** of the different interfaces corresponding to the different added networks, with the names **udev rules**.

**udev rules** associate the name of the original device to the name you want to modify.

It can be done with this command and with each of the names described in the ```NAME``` field:
```
sudo bash -c 'cat   /etc/udev/rules.d/70-persistent-net.rules   EOF
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp1s0", NAME="output"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp2s0", NAME="cirvianum1"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp3s0", NAME="cirvianum2"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp4s0", NAME="cirvianum3"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp5s0", NAME="cirvianum4"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp6s0", NAME="cirvianum5"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp7s0", NAME="personal"
SUBSYSTEM=="net", ACTION="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp8s0", NAME="vpn"
EOF'
```
To apply changes you need to make a **reset**. 
```
isard:ubuntu:$$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state unknown mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00 brd 00:00:00:00:00:00:00
2: output: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff:ff
    altname enp1s0
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:35:1d brd ff:ff:ff:ff:ff:ff
    altname enp2s0
4: cirvianum2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3a:5e:b9 brd ff:ff:ff:ff:ff:ff
    altname enp3s0
5: cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3b:75:c9 brd ff:ff:ff:ff:ff:ff
    altname enp4s0
6: cirvianum4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:03:c8:83 brd ff:ff:ff:ff:ff:ff
    altname enp5s0
7: cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:41:76:68 brd ff:ff:ff:ff:ff:ff
    altname enp6s0
8: Personal: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:19:cb:5f brd ff:ff:ff:ff:ff:ff:ff
    altname enp7s0
9: vpn: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1366 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:de:9f brd ff:ff:ff:ff:ff:ff:ff
    altname enp8s0
```

: NOTE: If **later** are **removed**, **added**, or **change** the **order** of the cards corresponding to the interfaces, we will have to **re-modified** this **udev rule file**.
This setting works for the current **any Linux distribution**.


### Configuring each card and running each

For the explanation, **two desktops** configured with **same networks**, named **team1** and **team2** will be used. We will put the same networks into both desktops to make **card connection tests** and see which **allow** communication and which do not, **how** this communication is set, and which **addresses** are configured in each depending on the local network.

![](./network_tests.images/5YE64YE.png)


#### Network default - internet output

"**Default**" allows desktops **Internet exit** with each **isolated IP address** from the rest (desktops **do not communicate**). It is used by default in **all** Isard base templates.

Being the first card, Ubuntu Desktop's internal DHCP has created a configuration for it (behavior that depends on each distribution, there is no need to be equal to each Linux desktop).

```
isard1:team1:$$ ip to s output
2: output: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:44:60:ce brd ff:ff:ff:ff:ff:ff
    altname enp1s0
    inet 192.168.121.141/22 brd 192.168.123.255 scope global dynamic output
       valid validlft 3493sec preferred_lft 3493sec
    inet6 fe80::c184:3254:a9dc:1837/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

isard2team2:$$ ip a s output
2: output: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff:ff
    altname enp1s0
    inet 192.168.121.176/22 brd 192.168.123.255 scope global dynamic noprefixroute output
       valid 30lft 3068sec preferred_lft 3068sec
    inet6 fe80::2cd1:aa3f:8fe3:9268/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```


#### Private Networks

**private** networks are meant to mount scenarios where **different users** share a **same network**. They work on an Ethernet segment **isolated** and there is no DHCP server that gives IP addresses. It is therefore recommended to either **add a DHCP server** to these networks or to configure the IP address **manually**.


###### Unmanaged

For new network cards that do not have a configuration, **NetworkManager** will try to create a new connection. 
To slow down this behavior, it is possible to slow down NetworkManager and its work that **automatically** creates connections, and to make the "**unmanaged** card".

One way to do this is through rules in the file ```/etc/NetworkManager/conf.d/99-unmanaged-devices.conf```: 

```
sudo bash -c 'cat . /etc/NetworkManager/conf.d/99-unmanaged-devices.conf   EOF
[keyfile]
unmanaged-devices=interface-name:cirvianum1;interface-name:cirvianum2
EOF'
```


##### Disabled

A network configuration is "**disabled**" when **disabled from the system** and will not be used. 
It can be done with **nmtui**, **nmcli** or with the graphical connection manager.

```
nmcli con add con-name cirvianum3 ifname cirvianum3 type ethernet ipv4.method disabled ipv6.method disabled connection.autoconnect yes
nmcli con add con-name cirvianum4 ifname cirvianum4 type ethernet ipv4.method disabled ipv6.method disabled connection.autoconnect yes
```


###### Dynamic DHCP IP Server

This setting can be done by leaving the card ready with this command and/or **taking advantage of a template** that already carries a ready DHCP server.

```
sudo nmcli con add con-name cirvianum5 ifname cirvianum5 type ethernet method auto connection.autoconnect yes
```

![](./network_tests.images/aPCbWTY.png)

![](./network_tests.images/I0JdCAn.png)

You can **edit the DHCP server configuration** file and **restart** service, so that it takes the appropriate settings for practice cards and networking.


###### Personal

They are intended to perform practices where the user uses an exclusive **network segment** among **seus personal desktops**; they will not be able to see peer desktops even if they are **all connected** to this same “**Personal** network”.

Templates with fixed IP addresses can be left with this network and **will not collide between users**. 

For example, we will configure the fixed ip of family 172.18.0.101/24:

```
sudo nmcli con add con-name personal ifname personal type ethernet ip4 172.18.0.101/24 connection.autoconnect yes
```


##### VPN

Designed to work **from home**, you can consult the official manual to see [how VPN is configured on different client systems](https://isard.gitlab.io/isardvdi-docs/user/vpn.ca/#vpn).

It is the network used by the **RDP viewers**, and has a smaller MTU because **encrypted** packages are encapsulated.


### Base Card Verification

Rebooting and checking the networks.

- "**output**" can exit **Internet** independently:

```
isard11team11:$$ ip r s default
default via 192.168.120.1 dev output proto dhcp metric 102 

isard11team11:$$ ping -c 1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp8seq=1 ttl=120 time=0.916 ms
```

Check that devices are **managed by NetworkManager** except "**cirvianum1**" and "**cirvianum2**":

```
isard11team11:$$ sudo nmcli device
DEVICE TYPE STATE CONNECTION 
ethernet output connected output    
cirvianum5 ethernet connected cirvianum5 
personal ethernet connected   
vpn ethernet connected vpn        
cirvianum3 ethernet connected cirvianum3 
cirvianum4 ethernet connected cirvianum4 
cirvianum1 ethernet unmanaged --         
cirvianum2 ethernet unmanaged --         
unmanaged loopback -- 
```

The final step with this desktop is to **create a template** and thus not have to configure different networks for future practices.


### Practice with two desktops

Create the desktops **team188nets** and **team288nets** based on the **same template** created in the previous step and booted together with a desktop that will act as ** DHCP server**:

![](./network_tests.images/Z8TDhgC.png)

- From **team2** you can ping the team Personal of **team1**:

```
isard.team2:$$ ping -c 1 172.18.0.101
PING 172.18.0.101 (172.18.0.101) 56(84) bytes of data.
64 bytes from 172.18.0.101: icmp.seq=1 ttl=64 time=0.038 ms
```

- The card "**cirvanium5**" on the **team1** can be accessed with a dynamic IP:

```
isard2team2:$$ ip a s cirvianum5
7: cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:0c:ed:38 brd ff:ff:ff:ff:ff:ff
    altname enp6s0
    inet 192.168.220.109/23 brd 192.168.221.255 scope global dynamic noprefixroute cirvianum5
       validlflft 6844sec preferred_lft 6844sec
    inet6 fe80::fb30:446:f480:f5fd/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

isard@team2:$$ ping -c 1 192.168.220.108
PING 192.168.220.108 (192.168.220.108) 56(84) bytes of data.
64 bytes from 192.168.220.108: icmp.seq=1 ttl=64 time=13.1 ms
```

- An IP address **fixed** to "**cirvianum3**" in **equiple1** can be configured using **graphic environment**, for example address 192.168.11.101/24:

![](./network_tests.images/xxqdDlg.png)

- Do the same on **team2** with **nmtui** interface:

```
sudo nmtui
```

![](./network_tests.images/CYTWdoZ.png)

![](./network_tests.images/Gi7dOkj.png)

![](./network_tests.images/biS9KYy.png)

![](./network_tests.images/UY5cYtd.png)

```
isard2team2:$$ ip a s cirvianum3
5: cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:44:94:bb brd ff:ff:ff:ff:ff:ff
    altname enp4s0
    inet 192.168.11.102/24 brd 192.168.11.255 scope global noprefixroute cirvianum3
       valid_lft forever preferred_lft forever


isard@team2:$$ ping -c 1 192.168.11.101
PING 192.168.11.101 (192.168.11.101) 56(84) bytes of data.
64 bytes from 192.168.11.101: icmp1:seq=1 ttl=64 time=11.0 ms
```

- Finally, the **cirvianum1** and **cirvianum2** networks are out of NetworkManager control and have booted ***down***, as seen here with **cirvanium1**:

```
isard1team2:$$ ip link show cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff:ff
    altname enp2s0
```

They are activated **manually** and configured with the terminal without interference with the NetworkManager service.

Two IP addresses corresponding to each interface, of **different families and classes**, are raised and communication is done:

```
isard1team1:$$ sudo ip link set cirvianum1 up
isard1:team1:1$ sudo ip a at 192.168.21.101/24 dev cirvianum1
isard1:team1:1$ sudo ip a 172.19.0.101/16 dev cirvianum1
isard1:team1:$$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:7b:79:ee brd ff:ff:ff:ff:ff:ff
    altname enp2s0
    inet 192.168.21.101/24 scope global cirvianum1
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe7b:79ee/64 scope link 
       valid_lft forever preferred_lft forever
```

```
isard1team2:$$ sudo ip link set cirvianum1 up
isard2team2:$$ sudo ip a at 192.168.21.102/24 dev cirvianum1
isard1team2:$$ sudo ip a 172.19.0.102/16 dev cirvianum1
isard1team2:$$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff:ff
    altname enp2s0
    inet 192.168.212/24 scope global cirvianum1
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe61:ffbd/64 scope link 
       valid_lft forever preferred_lft forever

isard@team2:$$ ping -c 1 192.168.21.101
PING 192.168.21.101 (192.168.21.101) 56(84) bytes of data.
64 bytes from 192.168.21.101: icmp=seq=1 ttl=64 time=11.1 ms

isard@team2:$$ ping -c 1 172.19.0.101
PING 172.19.0.101 (172.19.0.101) 56(84) bytes of data.
64 bytes from 172.19.0.101: icmp1:seq=1 ttl=64 time=0.263 ms

isard1team2::$ sudo ip address flush dev cirvianum1
isard1team2:$$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff:ff
    altname enp2s0

```