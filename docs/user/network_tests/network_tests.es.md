# Pruebas con redes

Las **redes** a *IsardVDI dan lugar a un montón de **escenarios diferentes** por **cada tipo de práctica**.

En este manual se explican los **diferentes tipos de redes** que se pueden configurar a los escritorios virtuales y **ejemplos de casos de uso** por cada tipo.

Primeramente se **añaden** todas las redes en un escritorio y conforme se configuran se **describe** su **funcionamiento** y **casos de uso**.


## Manual de usuario

### Asignación de redes al escritorio

1.- Se crea un escritorio en base a una plantilla, o en un escritorio ya creado se [modifica el apartado de Redes a Hardware](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware), y se añaden **ocho redes diferentes**

![](./network_tests.images/crear_escriptori.png)

![](./network_tests.images/redes.png)

2.- Se **arranca** el escritorio y se abre un **terminal** donde se comprueban las 8 redes añadidas, escribiendo ```*ip link show```:
```
isard@ubuntu:~$ *ip link show
1: *lo: <LOOPBACK,UP,LOWER_UP> *mtu 65536 *qdisc *noqueue *state *UNKNOWN modo *DEFAULT *group *default *qlen 1000
 link/loopback 00:00:00:00:00:00 *brd 00:00:00:00:00:00
2: *enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:*3d:82:*1e *brd *ff:*ff:*ff:*ff:*ff:*ff
3: *enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:25:35:*1d *brd *ff:*ff:*ff:*ff:*ff:*ff
4: *enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:3.ª:*5e:*b9 *brd *ff:*ff:*ff:*ff:*ff:*ff
5: *enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:*3b:75:*c9 *brd *ff:*ff:*ff:*ff:*ff:*ff
6: *enp5s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:03:*c8:83 *brd *ff:*ff:*ff:*ff:*ff:*ff
7: *enp6s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:41:76:68 *brd *ff:*ff:*ff:*ff:*ff:*ff
8: *enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:19:*cb:*5f *brd *ff:*ff:*ff:*ff:*ff:*ff
9: *enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1366 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:25:de:*9f *brd *ff:*ff:*ff:*ff:*ff:*ff
```

> NOTA: El **orden** de tarjetas de red que se configuran en el escritorio al IsardVDI es la orden de tarjetas que aparecen configuradas al sistema del escritorio, por ejemplo, cuando se escribe al terminal el mando ***ip -c a**


#### renombramiento tarjetas de red

Para **facilitar** la experiencia y **agilizar** el trabajo del usuario, se pueden **modificar los nombres** de las diferentes interfaces que corresponden a las diferentes redes añadidas, con las nombradas **reglas *udev**.

Las **reglas *udev** asocian el nombre del dispositivo original al nombre que se *volgui modificar.

Se puede hacer con este mando y con cada uno de los nombres descritos en el campo ```*NAME```:
```
*sudo *bash -c '*cat > /etc/udev/rules.d/70-persistente-neto.*rules << *EOF
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp1s0", *NAME="salida"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp2s0", *NAME="*cirvianum1"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp3s0", *NAME="*cirvianum2"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp4s0", *NAME="*cirvianum3"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp5s0", *NAME="*cirvianum4"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp6s0", *NAME="*cirvianum5"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp7s0", *NAME="personal"
*SUBSYSTEM=="limpio", *ACTION=="*add", *DRIVERS=="?*", *ATTR{*type}=="1", *KERNEL=="*enp8s0", *NAME="*vpn"
*EOF'
```
Para aplicar los cambios es necesario hacer un **reinicio**. 
```
isard@ubuntu:~$ *ip link show
1: *lo: <LOOPBACK,UP,LOWER_UP> *mtu 65536 *qdisc *noqueue *state *UNKNOWN modo *DEFAULT *group *default *qlen 1000
 link/loopback 00:00:00:00:00:00 *brd 00:00:00:00:00:00
2: salida: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:*3d:82:*1e *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp1s0
3: *cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:25:35:*1d *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp2s0
4: *cirvianum2: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:3.ª:*5e:*b9 *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp3s0
5: *cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:*3b:75:*c9 *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp4s0
6: *cirvianum4: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:03:*c8:83 *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp5s0
7: *cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:41:76:68 *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp6s0
8: personal: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:19:*cb:*5f *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp7s0
9: *vpn: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1366 *qdisc *fq_*codel *state *UP modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:25:de:*9f *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp8s0
```

> NOTA: Si **posteriormente** se **sacan**, **añaden**, o **cambian** la **orden** de las tarjetas correspondientes a las interfaces tendremos que **volver a modificar** este **fichero de reglas *udev**.
Esta configuración funciona para **cualquier distribución de Linux** actual.


### Configuración de cada tarjeta y funcionamiento de cada una

Por la explicación se usarán **dos escritorios** configurados con las **mismas redes**, nombrados ***equip1** y ***equip2**. meteremos las mismas redes en ambos escritorios para hacer **pruebas de conexión de tarjetas** y ver cuáles **permiten** comunicación y qué no, **como** se establece esta comunicación, y qué **direcciones** se configuran en cada una dependiendo de la red local.

![](./network_tests.images/5YE64YE.png)


#### Red *default - salida a internet

"***Default**" permite a los escritorios **salida a Internet** con cada **dirección IP aislada** del resto (escritorios **no se comunican**). Se usa por defecto a **todas** las plantillas base de IsardVDI.

Al ser la primera tarjeta, el *DHCP interno de *Ubuntu *Desktop le ha creado una configuración (comportamiento que depende de cada distribución, no hay porque ser igual a cada escritorio Linux).

```
isardvdi@equip1:~$ *ip a s salida
2: salida: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:44:60:ce *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp1s0
 *inet 192.168.121.141/22 *brd 192.168.123.255 *scope global *dynamic salida
 *valid_*lft *3493sec *preferred_*lft *3493sec
 *inet6 *fe80::*c184:3254:*a9dc:1837/64 *scope link *noprefixroute 
 *valid_*lft *forever *preferred_*lft *forever

isardvdi@equip2:~$ *ip a s salida
2: salida: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:*3d:82:*1e *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp1s0
 *inet 192.168.121.176/22 *brd 192.168.123.255 *scope global *dynamic *noprefixroute salida
 *valid_*lft *3068sec *preferred_*lft *3068sec
 *inet6 *fe80::*2cd1:*aa3f:*8fe3:9268/64 *scope link *noprefixroute 
 *valid_*lft *forever *preferred_*lft *forever
```


#### Redes privadas

Las redes **privadas** están pensadas para montar escenarios donde **diferentes usuarios** comparten una **misma red**. Trabajan en un segmento de *Ethernet **aislado** y no hay un servidor *DHCP que dé direcciones IP. Por lo tanto, es recomendable o bien **añadir un servidor *DHCP** en estas redes o configurar la dirección IP **manualmente**.


##### *Unmanaged

Por las tarjetas de red nuevas que no tengan una configuración, ***NetworkManager** mirará de crear una nueva conexión. 
Para frenar este comportamiento es posible frenar el *NetworkManager y su trabajo que hace **automáticamente** de crear conexiones, y hacer la tarjeta "***unmanaged**".

Una forma de hacerlo es mediante reglas al fichero ```/etc/NetworkManager/conf.d/99-*unmanaged-*devices.*conf```: 

```
*sudo *bash -c '*cat > /etc/NetworkManager/conf.d/99-*unmanaged-*devices.*conf << *EOF
[*keyfile]
*unmanaged-*devices=*interface-*name:*cirvianum1;*interface-*name:*cirvianum2
*EOF'
```


##### *Disabled

Una configuración de red es "***disabled**" cuando se **deshabilita del sistema** y no se hará uso. 
Se puede hacer con ***nmtui**, ***nmcli** o con el gestor de conexiones gráfico.

```
*nmcli cono *add cono-*name *cirvianum3 *ifname *cirvianum3 *type *ethernet *ipv4.*method *disabled *ipv6.*method *disabled *connection.*autoconnect *yes
*nmcli cono *add cono-*name *cirvianum4 *ifname *cirvianum4 *type *ethernet *ipv4.*method *disabled *ipv6.*method *disabled *connection.*autoconnect *yes
```


##### Servidor *DHCP e iP dinámica

Se puede hacer esta configuración dejando la tarjeta preparada con este mando y/o **aprovechar una plantilla** que ya lleve un servidor *DHCP preparado.

```
*sudo *nmcli cono *add cono-*name *cirvianum5 *ifname *cirvianum5 *type *ethernet *method auto *connection.*autoconnect *yes
```

![](./network_tests.images/aPCbWTY.png)

![](./network_tests.images/I0JdCAn.png)

Se puede **editar el fichero** de configuración del servidor *DHCP y **reiniciar** el servicio, para que coja la configuración adecuada para las tarjetas y red de la práctica.


##### Personales

Están pensadas para realizar prácticas donde el usuario use un **segmento de red exclusivo** entre los **suyos escritorios personales**; no podrá ver los escritorios de compañeros aunque estén **todos conectados** en esta misma red "**Personal**".

Se pueden dejar configuradas plantillas con direcciones IP fijas con esta red y **no colisionarán entre usuarios**. 

Por ejemplo configuraremos la *ip fija de la familia 172.18.0.101/24:

```
*sudo *nmcli cono *add cono-*name personal *ifname personal *type *ethernet *ip4 172.18.0.101/24 *connection.*autoconnect *yes
```


##### VPN

Pensada para trabajar **desde casa**, se puede consultar el manual oficial para ver [como se configura la VPN en los diferentes sistemas del cliente](https://isard.gitlab.io/isardvdi-docs/user/vpn.ca/#*vpn).

Es la red que se usa por los **visores de *RDP**, y tiene un *MTU más pequeño debido a que se encapsulan los paquetes **cifrados**.


### Verificación de las tarjetas base

Se hace un reinicio y se verifican las redes.

- "**salida**" puede salir a **Internet** independientemente:

```
isardvdi@equip11:~$ *ip r s *default
*default vía 192.168.120.1 *dev salida *proto *dhcp *metric 102 

isardvdi@equip11:~$ *ping -c 1 8.8.8.8
*PING 8.8.8.8 (8.8.8.8) 56(84) bytes *of fecha.
64 bytes *from 8.8.8.8: *icmp_*seq=1 *ttl=120 *time=0.916 ms
```

Se comprueba que los dispositivos son **gestionados por el *NetworkManager** excepto "***cirvianum1**" y "***cirvianum2**":

```
isardvdi@equip11:~$ *sudo *nmcli *device
*DEVICE *TYPE *STATE *CONNECTION 
salida *ethernet conectado salida 
*cirvianum5 *ethernet conectado *cirvianum5 
personal *ethernet conectado personal 
*vpn *ethernet conectado *vpn 
*cirvianum3 *ethernet conectado *cirvianum3 
*cirvianum4 *ethernet conectado *cirvianum4 
*cirvianum1 *ethernet sin gestión -- 
*cirvianum2 *ethernet sin gestión -- 
*lo *loopback sin gestión -- 
```

El paso final con este escritorio es **crear una plantilla** y así no tener que configurar las diferentes redes por futuras prácticas.


### Práctica con dos escritorios

Se crean los escritorios ***equip1_*8xarxes** y ***equip2_*8xarxes** en base a la **misma plantilla** creada al paso anterior y se arrancan junto con un escritorio que hará de **servidor *DHCP**:

![](./network_tests.images/Z8TDhgC.png)

- Desde el ***equip2** se puede hacer *ping a la "**Personal**" del ***equip1**:

```
isardvdi@equip2:~$ *ping -c 1 172.18.0.101
*PING 172.18.0.101 (172.18.0.101) 56(84) bytes *of fecha.
64 bytes *from 172.18.0.101: *icmp_*seq=1 *ttl=64 *time=0.038 ms
```

- Se puede acceder a la tarjeta "***cirvanium5**" de la ***equip1** con una IP dinámica:

```
isardvdi@equip2:~$ *ip a s *cirvianum5
7: *cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:*0c:*ed:38 *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp6s0
 *inet 192.168.220.109/23 *brd 192.168.221.255 *scope global *dynamic *noprefixroute *cirvianum5
 *valid_*lft *6844sec *preferred_*lft *6844sec
 *inet6 *fe80::*fb30:446:*f480:*f5fd/64 *scope link *noprefixroute 
 *valid_*lft *forever *preferred_*lft *forever

isardvdi@equip2:~$ *ping -c 1 192.168.220.108
*PING 192.168.220.108 (192.168.220.108) 56(84) bytes *of fecha.
64 bytes *from 192.168.220.108: *icmp_*seq=1 *ttl=64 *time=13.1 ms
```

- Se puede configurar una dirección IP **fija** a "***cirvianum3**" de la ***equip1** mediante **entorno gráfico**, por ejemplo la dirección 192.168.11.101/24:

![](./network_tests.images/xxqdDlg.png)

- Se hace el mismo al ***equip2** con la interfaz ***nmtui**:

```
*sudo *nmtui
```

![](./network_tests.images/CYTWdoZ.png)

![](./network_tests.images/Gi7dOkj.png)

![](./network_tests.images/biS9KYy.png)

![](./network_tests.images/UY5cYtd.png)

```
isardvdi@equip2:~$ *ip a s *cirvianum3
5: *cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:44:94:*bb *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp4s0
 *inet 192.168.11.102/24 *brd 192.168.11.255 *scope global *noprefixroute *cirvianum3
 *valid_*lft *forever *preferred_*lft *forever


isardvdi@equip2:~$ *ping -c 1 192.168.11.101
*PING 192.168.11.101 (192.168.11.101) 56(84) bytes *of fecha.
64 bytes *from 192.168.11.101: *icmp_*seq=1 *ttl=64 *time=11.0 ms
```

- Finalmente, las redes ***cirvianum1** y ***cirvianum2** están fuera del control del *NetworkManager y han arrancado ****down***, como se ve aquí con ***cirvanium1**:

```
isardvdi@equip2:~$ *ip link show *cirvianum1
3: *cirvianum1: <BROADCAST,MULTICAST> *mtu 1500 *qdisc *noop *state *DOWN modo *DEFAULT *group *default *qlen 1000
 link/ether 52:54:00:61:*ff:*bd *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp2s0
```

Se activan **manualmente** y configuran con el terminal sin interferencias con el servicio de *NetworkManager.

Se **levantan** y configuran dos direcciones IP correspondientes para cada interfaz, de **diferentes *families y clases**, y se efectúa la comunicación:

```
isardvdi@equip1:~$ *sudo *ip link siete *cirvianum1 *up
isardvdi@equip1:~$ *sudo *ip a a 192.168.21.101/24 *dev *cirvianum1
isardvdi@equip1:~$ *sudo *ip a a 172.19.0.101/16 *dev *cirvianum1
isardvdi@equip1:~$ *ip a s *cirvianum1
3: *cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:*7b:79:*ee *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp2s0
 *inet 192.168.21.101/24 *scope global *cirvianum1
 *valid_*lft *forever *preferred_*lft *forever
 *inet6 *fe80::5054:*ff:*fe7b:*79ee/64 *scope link 
 *valid_*lft *forever *preferred_*lft *forever
```

```
isardvdi@equip2:~$ *sudo *ip link siete *cirvianum1 *up
isardvdi@equip2:~$ *sudo *ip a a 192.168.21.102/24 *dev *cirvianum1
isardvdi@equip2:~$ *sudo *ip a a 172.19.0.102/16 *dev *cirvianum1
isardvdi@equip2:~$ *ip a s *cirvianum1
3: *cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:61:*ff:*bd *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp2s0
 *inet 192.168.21.102/24 *scope global *cirvianum1
 *valid_*lft *forever *preferred_*lft *forever
 *inet6 *fe80::5054:*ff:*fe61:*ffbd/64 *scope link 
 *valid_*lft *forever *preferred_*lft *forever

isardvdi@equip2:~$ *ping -c 1 192.168.21.101
*PING 192.168.21.101 (192.168.21.101) 56(84) bytes *of fecha.
64 bytes *from 192.168.21.101: *icmp_*seq=1 *ttl=64 *time=11.1 ms

isardvdi@equip2:~$ *ping -c 1 172.19.0.101
*PING 172.19.0.101 (172.19.0.101) 56(84) bytes *of fecha.
64 bytes *from 172.19.0.101: *icmp_*seq=1 *ttl=64 *time=0.263 ms

isardvdi@equip2:~$ *sudo *ip *address *flush *dev *cirvianum1
isardvdi@equip2:~$ *ip a s *cirvianum1
3: *cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> *mtu 1500 *qdisc *fq_*codel *state *UP *group *default *qlen 1000
 link/ether 52:54:00:61:*ff:*bd *brd *ff:*ff:*ff:*ff:*ff:*ff
 *altname *enp2s0

```