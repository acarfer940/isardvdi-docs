# VPN

## What is the purpose of VPN connection in IsardVDI?

The user establishes a **VPN tunnel** in IsardVDI to **access their desktops using the [RDP VPN viewer](../viewers/viewers/#how-to-use-rdp-and-rdp-vpn-viewers)** or simply to **establish communication between the host machine and user desktops** (e.g., accessing them via an SSH connection).

## How to establish the user VPN tunnel

Firstly, the **Wireguard VPN** network interface must be active on all the desktops that need to be visible in the VPN tunnel, either by [editing their "Hardware"](../edit_desktop/#edit-desktop) or [creating them directly with that network interface](../create_desktop/#main-view).

![](vpn.images/1.png){width="35%"}

Secondly, access the user's dropdown menu and select the **VPN** option, which will download a ***.conf*** file to the host machine.

![](vpn.images/2.png){width="70%"}

To establish the connection, the **WireGuard** program is used. Depending on the host machine's operating system, download, install, and use the program as explained below.

### With Linux host

After downloading the VPN ***.conf*** file, run the following commands in the terminal:

```
$ sudo apt install wireguard
$ sudo mv /download/path/isard-vpn.conf /etc/wireguard
$ wg-quick up isard-vpn
```

The user's VPN tunnel is now properly established. To test its functionality, you can **start the user's desktops** and **ping** their IP addresses from the host machine:

![](vpn.images/3.png){width="30%"}

```
root@debian:~# ping 10.2.91.193
PING 10.2.91.193 (10.2.91.193) 56(84) bytes of data.
64 bytes from 10.2.91.193: icmp_seq=1 ttl=63 time=13.6 ms
64 bytes from 10.2.91.193: icmp_seq=2 ttl=63 time=6.15 ms
64 bytes from 10.2.91.193: icmp_seq=3 ttl=63 time=6.46 ms
64 bytes from 10.2.91.193: icmp_seq=4 ttl=63 time=6.89 ms
^C
--- 10.2.91.193 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 6.146/8.275/13.601/3.085 ms
```

### With Windows host

Download **[WireGuard for Windows using this link](https://download.wireguard.com/windows-client/wireguard-installer.exe)**, open the program, and import the recently downloaded VPN ***.conf*** file.

![](vpn.images/4.png){width="60%"}

![](vpn.images/5.png){width="60%"}

![](vpn.images/6.png){width="60%"}

The user's VPN tunnel is now properly established. To test its functionality, you can **start the user's desktops** and **ping** their IP addresses from the host machine:

![](vpn.images/3.png){width="30%"}

```
C:\Users\windows>ping 10.2.91.193

Pinging 10.2.91.193 with 32 bytes of data:
Reply from 10.2.91.193: bytes=32 time=13ms TTL=116
Reply from 10.2.91.193: bytes=32 time=14ms TTL=116
Reply from 10.2.91.193: bytes=32 time=14ms TTL=116
Reply from 10.2.91.193: bytes=32 time=14ms TTL=116

Ping statistics for 10.2.91.193:
    Packets: Sent = 4, Received = 4, Lost = 0
    (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 13ms, Maximum = 14ms, Average = 13ms

```
