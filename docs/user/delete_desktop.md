# Delete desktop

To delete a desktop, press the button ![](./delete_desktop.es.images/delete_desktop1.png)

![](./delete_desktop.images/delete_desktop1.png){width="80%"}

![](./delete_desktop.images/delete_desktop2.png){width="80%"}
