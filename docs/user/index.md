# Introduction

The user manual is organized following the order in the IsardVDI web menu, and consists in:

- **Login** methods available to login into the system.
- **Desktops**, it's actions and viewers.
- **Templates** that allows you to replicate as multiple desktops.
- **Media** files can be uploaded, usually an ISO to boot a new desktop.
- **Deployments** allows supervising desktops while working on it.
- **Bookings** are needed for resources like vGPU.
- **Storage** shows you how much your desktops disks are used.
- **Administration** is a link that allows managers and admins to do advanced functions.
- **Profile** not only shows your info and allows to reset password, it also shows your quota and usage.

![](index.images/global_view.png)
