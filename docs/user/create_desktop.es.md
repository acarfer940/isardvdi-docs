# Inicio rápido

Hay dos tipos de escritorios:

* **Temporal**: Cuando se apague un escritorio, **se perderá** todo lo que se haya hecho, guardado o descargado.
* **Persistente**: Cuando se apague, **no se perderá** la información o programas que se hayan instalado. 

## Vista principal

Para crear un escritorio se pulsa el botón ![](./create_desktop.es.images/creacion2.png){width="8%"} de la vista **Escritorios**.

![](./create_desktop.es.images/creacion1.png){width="80%"}

Se rellenan los campos, se **selecciona una plantilla** y se pulsa el botón ![](./create_desktop.es.images/creacion4.png){width="6%"}.

![](./create_desktop.es.images/creacion3.png){width="80%"}

Se puede cambiar la configuración del escritorio en el apartado de **"Opciones avanzadas"**:

!!! info inline end "Enlaces"
        <font size="3">
            <ul>
            <li> [Visores](../user/edit_desktop.es.md/#visores)</li>
            <li> [Login RDP](../user/edit_desktop.es.md/#login-rdp)</li>
            <li> [Hardware](../user/edit_desktop.es.md/#hardware)</li>
            <li> [Reservables](../user/edit_desktop.es.md/#reservables)</li>
            <li> [Medios](../user/edit_desktop.es.md/#media)</li>
            </ul>
        </font>

![](./create_desktop.es.images/creacion5.png){width="60%"}
![](create_desktop.es.images/login_rdp.png){width="60%"}
![](create_desktop.es.images/hardware.png){width="60%"}
![](create_desktop.ca.images/hardware_2.png){width="60%"}
![](create_desktop.es.images/media.png){width="60%"}
