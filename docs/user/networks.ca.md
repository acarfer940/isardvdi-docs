# Xarxes

Els escriptoris de l'IsardVDI poden tenir diverses interfícies de xarxa alhora i poden ser de diferents tipus:

- **Default**: proporciona una adreça dinàmica (DHCP) amb DNS i passarel·la a través d'un encaminador NAT. Té connexió a Internet i està aïllat d'altres escriptoris del sistema.
- **Personal**: el tipus de xarxa personal assignat als escriptoris d'usuari estarà a la mateixa xarxa (VLAN). Amb xarxes personals, els usuaris poden configurar un entorn de xarxa privat per als seus escriptoris virtuals, on es poden assignar les seves pròpies adreces IP, crear la configuració de la xarxa i controlar l'accés a la xarxa. Això pot ser útil per a escenaris com proves o entorns de desenvolupament, on múltiples escriptoris virtuals d'usuari necessiten interactuar entre ells en un entorn tancat.
- **Private (OVS)**: les xarxes **OpenVSwitch** permeten connectar a la mateixa xarxa (VLAN) tots els escriptoris virtuals que l'hagin connectat, independentment del propietari de l'escriptori virtual. També es poden connectar a la mateixa xarxa VLAN en infraestructura, permetent així connectar els ordinadors físics a escriptoris virtuals. **Aquesta opció s'ha de configurar a nivell de servidor. (Llegir "isardvdi.cfg" per a les opcions de configuració)**.
- **Wireguard VPN**: Aquesta és una xarxa del sistema que assigna una adreça IP a l'escriptori virtual i és necessària per a connectar-se a l'escriptori des de casa i permetre connexions de visualització de tipus **RDP**.

!!! Info
    Les [xarxes les crea l'usuari amb rol **administrador**](../admin/domains.md/#interfaces) del sistema i els altres usuaris hauran de demanar permís per crear-ne de noves

Casos d'ús amb diferents tipus d'interfícies de xarxa:

- [**Client - Entorn del servidor**](./client_server/client_server.ca.md): com crear una xarxa **Private (OVS)** i utilitzar-la entre el rol **advanced** i el rol **user**.
- [**Proves de xarxa**](./network_tests/network_tests.ca.md): s'introdueixen diferents xarxes i com configurar-les als escriptoris virtuals Linux.
- [**Xarxes personals**](./private_and_personal_networks/private_and_personal_networks.ca.md): com utilitzar les xarxes privades i les xarxes personals de l'usuari (des d'un usuari amb rol **administrador**).
