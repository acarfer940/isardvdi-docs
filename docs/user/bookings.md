# Bookings

In this section, we will see how to make bookings for vGPU-enabled desktops associated with NVIDIA GPU resources, how to add this type of hardware to virtual desktops, and the associated bookings management.

For more information, see the [GPU section](../deploy/gpus/gpus.md).


## Why a bookings system?

IsardVDI is designed so that users can create and deploy desktops autonomously. The limit of how many desktops can start simultaneously in the system is conditioned by the available resources. If the desktops do not have vGPUs, the limit usually comes from the amount of available RAM on the hypervisors, so it is advisable to ensure that the memory and vCPU capacity of these servers is always sufficient to sustain the concurrency of desktops. We can limit the amount of resources we grant to each user, group, or category with the quotas and limits that IsardVDI allows us to establish.

In the case of vGPUs, they are expensive resources (there is the cost of the card and the cost of licensing), and to access these resources, a bookings system has been established with the following characteristics:

- The administrator can **plan schedules with different profiles** applied for each available card, allowing a profile with a lot of dedicated memory and few users to be established at a certain time (e.g., launching a rendering), and another profile with little memory and many users at another time (e.g., conducting training)

- There is a granular **permission system** that allows defining which users have permission to use a specific card profile. It allows only a small group of users to access profiles with more memory

- A user can book a desktop for a specific time slot with a **minimum time prior to the booking** and a **maximum booking duration**. This allows bookings to be made with little advance notice and prevents slots from being booked for a limited number of hours

- An advanced user can make a **booking for a deployment**, booking as many units of GPU profiles as desktops the deployment contains

- There is also a **booking priority system**, which allows setting rules for some users to overwrite and revoke bookings from others. This option is useful if we want to encourage the GPU card to be used by many users but also want to ensure that certain groups never lack booking capacity, such as if a group uses a 3D design program constantly, and they can revoke bookings from other users through this function


## Prepare desktop/deployment

### Create/Edit a desktop/deployment to work with GPU

[Create](create_desktop.md)/[Edit desktop](edit_desktop.md) or [create new deployment](../advanced/deployments.md) and scroll down to the **Hardware** section.

It's important that the **Videos** configuration has the **Only GPU** option checked. In Windows OS desktops with Nvidia drivers installed, it's not compatible to have both a QXL video card and a vGPU card at the same time.

![](bookings.images/hardware_videos_only_gpu.png)

In the next section **Reservables** below, assign the desired GPU **profile**.

![](bookings.images/bookables_gpu_dropdown.png)

NOTE: *To book and start a desktop with a profile, the administrator must have previously scheduled it.*


### RDP Viewers Only

Desktops with GPU use the GPU card driver and therefore can **only be accessed through RDP viewers**.

![](bookings.images/rdp_viewers.png)

**IMPORTANT!** If there is a problem with the operating system and we never get an IP address, we won't be able to access it via RDP. We must always avoid this, but even more so in this section, force shutdown of the desktop from the IsardVDI interface because it can cause disk corruption (as it's equivalent to unplugging a powered-on computer), and prevent it from starting the operating system and obtaining an IP address, which may leave us without access to the desktop.


## Booking a desktop/deployment

It is necessary to make **bookings** for **desktops** with vGPU. The administrator will have planned GPU profiles on the available cards, and the user can make bookings as long as they have permissions and their vGPU profile matches an available booking.

To book a GPU for the desktop, access it through the last action icon on the desktop card.

To book a deployment, access it through the deployments panel, with the same button to the right of the deployment.

![](bookings.images/desktop_card_action_schedule.png)

![](bookings.images/deployments_panel_schedule.png)

This gives us access to the weekly **availability** view (we can change the view to monthly or daily), where we find two columns for each day of the week. In the left column, the availability for the card profile is displayed, and in the right column, the bookings we have for that desktop or deployment are displayed.

In the figure, it can be seen that there is **availability** during the week and **no bookings** have been made.

![](bookings.images/booking_panel_availability.png)

Bookings can be created using the button in the upper right corner ![](bookings.images/add_booking_button.png) or by clicking and dragging on the bookings column to select the desired time range. In the form that appears, we can adjust the date and time range for the booking we want to make.

![](bookings.images/add_booking_modal.png)

Once the booking is made, it will appear in the right column for each day.

![](bookings.images/booking_1_drag_panel.png)


## Editing or deleting a booking

To edit a booking, click on it and you will be able to modify the time ranges for the duration of that booking. It is not possible to modify a booking that is already in progress.
To delete a booking, click on the time slot and press the delete button that appears when editing it.

![](bookings.images/edit_and_delete_booking_modal.png)


## Video Tutorials

Here we offer two video tutorials that explain the previous configurations of a desktop or deployment, and how to make bookings for both options, using the **two booking methods available in IsardVDI**, in the first and second part respectively.

English subtitles available.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
