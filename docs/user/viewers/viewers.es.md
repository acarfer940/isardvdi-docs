# Visores

## Descarga directa de software

<table>
  <thead>
    <tr>
      <th>Sistema operativo</th>
      <th>Visor SPICE</th>
      <th>Visor RDP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td> Windows</td>
      <td>
        <a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11</a>
      </td>
      <td>No hace falta instalar nada</td>
    </tr>
    <tr>
      <td>Linux</td>
      <td>
        <code>sudo apt install virt-viewer / sudo dnf install virt-viewer</code>
      </td>
      <td>
        <code>sudo apt install remmina / sudo dnf install remmina</code>
      </td>
    </tr>
    <tr>
      <td>Mac OS</td>
      <td>Puedes seguir esta <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">guía de instalación</a></td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Microsoft Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Android</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">aSPICE</a></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Apple iOS</td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">aSPICE Pro</a></td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">Remote Desktop Mobile</a></td>
    </tr>
  </tbody>
</table>



## SPICE

![](viewers.es.images/visor_spice_1.png){width="30%"}


### Descripción

SPICE es un protocolo de comunicación para entornos virtuales que permite el acceso a la señal de vídeo, ratón y teclado hacia el escritorio como si se tuviera conexión a la pantalla, ratón y teclado de un equipo real. 

* **Ventajas**:
    * Es el propio motor de máquinas virtuales que usa Isard (Qemu-KVM) el que permite el acceso, independientemente del sistema operativo que esté corriendo.
    * Se puede usar este protocolo para ver toda la secuencia, desde el principio, del arranque del escritorio. 
    * No se necesita instalar ningún componente en el sistema operativo del escritorio virtual para poder crear una interacción. 
    * Es un protocolo que optimiza el ancho de banda de vídeo utilizado comprimiendo la señal y enviando sólo las zonas que varían de un frame a otro.

* **Inconvenientes**:
    * El cliente no viene instalado por defecto en ningún sistema operativo, la instalación es muy sencilla y la puede hacer cualquier usuario, pero en entornos corporativos o educativos las restricciones de permisos pueden dificultar la instalación.
    * La instalación en Windows, requiere de un programa extra para poder redirigir los puertos USB
    * La conexión se hace por un proxy HTTP utlizando un método "CONNECT". Este método, en algunos casos, es filtrado por algún cortafuegos o proxy intermedio.

A destacar de este visor:

* Latencia baja
* Audio integrado
* Opción de conexión de dispositivos


### Cómo usar el visor SPICE

Después de realizar la [descarga del software](#descarga-directa-de-software) necesario, en el arranque de un escritorio, se selecciona **Visor SPICE** y se abre el archivo descargado. Una vez abierto se podrá acceder al escritorio.

![](viewers.es.images/visor_spice_3.png){width="80%"}


### Escanear dispositivos USB

!!! Info
    **PARA ANFITRIONES WINDOWS**
    
    Si se tiene instalado **virt-viewer 11**, hay que realizar uno de estos pasos para activar el escaneo de USB:

    - Opción 1
        - Desinstalar **virt-viewer 11** e instalar esta versión de **[virt-viewer 7](https://releases.pagure.org/virt-viewer/virt-viewer-x64-7.0.msi)**
        - Instalar paquete **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**

    - Opción 2
        - Instalar paquete **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**
        - Cambiar el fichero "**libusb-1.0.dll**" en la ruta "**C:\Archivos de programa\VirtViewer v11.0-256**" por **[este otro](https://nextcloud.isardvdi.com/s/jSngRESwbYZHRks)**


#### virt-viewer 7

Se abre el visor SPICE del escritorio y se seleccionan las opciones **Fichero >> Selección del dispositivo USB** y el dispositivo a escanear.

![](viewers.es.images/visor_spice_4.png){width="70%"}

![](viewers.es.images/visor_spice_5.png){width="70%"}


#### virt-viewer 11 (Windows)

Se abre el visor SPICE del escritorio, se hace click al segundo botón de la ventana del visor y se selecciona el dispositivo a escanear.

![](viewers.ca.images/visor_spice_3.png){width="80%"}

![](viewers.ca.images/visor_spice_4.png){width="70%"}


## VNC en navegador

![](viewers.es.images/visor_en_el_navegador_1.png){width="30%"}


### Descripción

NoVNC es un protocolo que trabaja al mismo nivel que SPICE, pero al ser el protocolo más antiguo, la señal de vídeo no es tan óptima. Es el protocolo que se usa en el visor integrado en el navegador. A destacar de este visor:

* Funciona con cualquier navegador web moderno
* Latencia media
* Audio no disponible
* Opción de conexión de dispositivos no disponible


### Cómo usar el visor VNC en navegador

Se selecciona la opción **Visor VNC en navegador** de los visores disponibles de un escritorio arrancado.

!!! Info
    <span style="font-size: medium">Es necesario tener en cuenta que la opción de abrir ventanas emergentes, puede estar bloqueada en el navegador. Si es así, el visor en el navegador no se abrirá. 
    En este caso, por ejemplo para Firefox se puede arreglar de la siguiente forma:</span>

    <span style="font-size: medium">Aparecerá un mensaje en la pestaña donde actualizar esta configuración:</span>

    ![](viewers.es.images/visor_en_el_navegador_3.png)

    ![](viewers.es.images/visor_en_el_navegador_4.png){width="50%"}

Automáticamente abrirá el visor del escritorio en una pestaña nueva.

![](viewers.es.images/visor_en_el_navegador_2.png){width="80%"}


## RDP

![](viewers.es.images/visor_rdp_1.png){width="30%"}
![](viewers.es.images/visor_rdp_8.png){width="30%"}
![](viewers.es.images/visor_rdp_9.png){width="30%"}


### Descripción

#### RDP nativo

RDP es el protocolo que se usa por defecto para conectarse remotamente a un equipo de sistema operativo Windows. Estos visores no están disponible desde el arranque del escritorio, y hay que esperar a que en el proceso, el escritorio obtenga una dirección IP. 

* **Ventajas**: 
    * La mejor experiencia de usuario si el sistema operativo del escritorio virtual es Windows, y también si lo es la máquina anfitriona
    * Dicho lo cual, no es necesario instalar software adicional en los anfitriones Windows, ya que el Cliente de Escritorio Remoto viene integrado por defecto en el sistema operativo 
    * Es necesario para una buena experiencia de usuario usando vGPUs de NVIDIA sobre sistemas operativos Windows
  
* **Inconvenientes**: 
    * Si hay algún problema en el arranque del sistema operativo no accedes a la señal de pantalla (se solventa conectándose por los visores SPICE o en el navegador)

A destacar de estos visores:

- Latencia baja (mejor experiencia en escritorios con SO Windows)
- Audio integrado
- Opción de conexión de dispositivos
- Mejor experiencia al utilizar aplicaciones de diseño que precisan de vGPU
- Escritorios con vGPU sólo funcionan con visores RDP


#### RDP en el navegador

IsardVDI utiliza el servidor *Guacamole* que permite que los clientes RDP en HTML5 (cualquier navegador actual) se conecten a los guest de Windows de IsardVDI a través del puerto HTTPS predeterminado. Además, cuenta con audio a través del navegador.

* Funciona con cualquier navegador moderno
* Baja latencia
* Audio integrado
* Opción de conexión de dispositivos no disponible


### Cómo usar los visores RDP y RDP VPN

Como se ha comentado anteriormente **el visor RDP necesita de una dirección IP** donde conectarse para establecer la conexión por RDP, es por este motivo por el cual cuando arranca un escritorio, observamos un parpadeo donde aparecen los enlaces a los visores RDP como no seleccionables.

Una vez éste ha obtenido dirección IP, se le informa de ella al usuario mediante la interfaz y se activa el acceso a los visores RDP, RDP en el navegador y RDP VPN:

![](viewers.es.images/visor_rdp_2.png){width="25%"}
![](viewers.es.images/visor_rdp_3.png){width="25%"}

!!! Warning
    <span style="font-size: medium">El escritorio debe tener activada la interfaz ***Wireguard VPN*** para que los visores RDP puedan conectarse.</span>

!!! Info
    <span style="font-size: medium">El visor **RDP VPN** se usa exactamente igual que el visor RDP nativo, exceptuando que el usuario deberá establecer la **conexión VPN** entre IsardVDI y su equipo personal. Consulta **[cómo usar la VPN](https://isard.gitlab.io/isardvdi-docs/user/vpn.es/)** del manual.</span>


#### Con anfitrión Windows

En primer lugar, por lo general, Windows ya tiene integrado el Cliente de Escritorio Remoto en los nuevos sistemas.

Al seleccionar los visores RPD o RDP VPN, se descarga un fichero con extensión *.rdp*, el cual contiene la información para poder conectarse con el escritorio:

![](viewers.es.images/visor_rdp_10.png){width="40%"}

La primera vez que se realiza la conexión RDP en el anfitrión, éste informa con una alerta de seguridad, la cual se puede evitar marcando la casilla "**No volver a preguntarme sobre conexiones a este equipo**" para el futuro.

![](viewers.es.images/visor_rdp_11.png){width="60%"}

Las credenciales para acceder a los escritorios que Isard ofrece por defecto son:

* Usuario: **isard**
* Contraseña: **pirineus**

!!! Warning
    <span style="font-size: medium">Este usuario y contraseña corresponden a las credenciales del usuario creado en el S.O. del escritorio virtual al que se quiere acceder, para que éste pueda iniciar sesión en el equipo.</span>
    
    <span style="font-size: medium">De tener un escritorio con un usuario personalizado, se deben **[modificar estas credenciales en el escritorio desde la interfaz de Isard](../../edit_desktop.es/#login-rdp)**.</span>

![](viewers.es.images/visor_rdp_12.png){width="40%"}

Al acceder al escritorio nos pide confirmación para aceptar el certificado:

![](viewers.es.images/visor_rdp_13.png){width="45%"}

Y finalmente se abre el cliente y podemos interactuar con el escritorio.


#### Con anfitrión Linux

Después de realizar la [descarga del programa Remmina](#descarga-directa-de-software) mediante línea de comandos, en el arranque de un escritorio, se seleccionan las opciones **Visor RDP** o **Visor RDP VPN** y se abre el archivo descargado con el programa **Remmina** recién instalado. Una vez abierto se podrá acceder al escritorio.

![](viewers.es.images/visor_rdp_6.png){width="80%"}

![](viewers.es.images/visor_rdp_7.png){width="80%"}


### Escanear dispositivos USB

#### Con anfitrión Windows

Del fichero ***.rdp*** que se descargue al seleccionar **Visor RDP** o **Visor RDP VPN** de un escritorio arrancado, se modifica con el botón derecho en la ruta donde se halle.

![](viewers.es.images/visor_rdp_10.png){width="40%"}
![](viewers.es.images/visor_rdp_14.png){width="45%"}


**Recursos locales >> Más >> Seleccionar la unidad USB >> Conectar**

![](viewers.es.images/visor_rdp_15.png){width="40%"}
![](viewers.es.images/visor_rdp_16.png){width="40%"}

![](viewers.es.images/visor_rdp_17.png){width="40%"}
![](viewers.es.images/visor_rdp_18.png){width="40%"}

Al acceder al visor RDP, aparece el dispositivo en el listado de discos del escritorio. 

![](viewers.es.images/visor_rdp_19.png){width="80%"}


#### Con anfitrión Linux

Por el momento **no es posible escanear dispositivos USB mediante el cliente Remmina** para sistemas operativos Linux.


### Cómo usar el visor RDP en el navegador

De la misma forma que con el **[visor noVNC para el navegador](#en-el-navegador)**, se abrirá el visor en una pestaña nueva en el navegador, con acceso directo al escritorio.

!!! Warning
    <span style="font-size: medium">El escritorio debe tener activada la interfaz ***Wireguard VPN*** para que los visores RDP puedan conectarse.</span>


## Activar RDP

Así como debe estar activado en el equipo anfitrión, como se ha explicado anteriormente en este manual, el protocolo RDP debe estar activado también dentro del sistema operativo de los propios escritorios virtuales.

!!! Tip
    <span style="font-size: medium">Las **plantillas prediseñadas que ofrece IsardVDI** ya tienen el sistema operativo **preparado para las conexiones Wireguard** con los visores RDP, por lo que este apartado del manual **no es necesario** si el usuario crea escritorios en base a estas plantillas.</span>

    <span style="font-size: medium">De lo contrario, si el usuario ha creado un **escritorio personalizado por su cuenta**, aquí se explica cómo configurar su sistema para poder acceder mediante los visores RDP.</span>

### Para escritorios con Windows

**1- Activar RDP**

![](viewers.es.images/guest_rdp_cfg1.png){width="70%"}

**2- Deshabilitar la autenticación de red**

![](viewers.es.images/guest_rdp_cfg2.png){width="40%"}

El escritorio está preparado.


### Para escritorios con Linux

Se puede seguir **[este apartado del manual para sistemas operativos Ubuntu](../../../guests/ubuntu_22.04/desktop/activate_rdp/configuration.ca)**.


## Detalles técnicos

En la VDI (Infraestructura de Escritorio Virtual), los visores son importantes porque permiten el acceso remoto a escritorios virtuales. VDI permite a los usuarios acceder a un escritorio virtual desde cualquier dispositivo con conexión a Internet, y los visores proporcionan la interfaz para que el usuario interactúe con ese escritorio virtual.

Los visores suelen ofrecer características como protocolos de visualización, compresión, redirección multimedia, redirección de dispositivos USB y autenticación. Permiten a los usuarios acceder al entorno de escritorio virtual, utilizar aplicaciones y acceder a datos desde una ubicación remota como si estuvieran físicamente sentados en el escritorio.

Los visores también desempeñan un papel fundamental en garantizar que la experiencia del escritorio virtual sea lo más parecida posible a la experiencia del equipo anfitrión. Deben ser capaces de manejar gráficos de alta resolución, contenido multimedia y otros tipos de datos sin causar retrasos o interrupciones en la experiencia del usuario.

En resumen, los visores son esenciales en VDI porque proporcionan los medios para que los usuarios accedan e interactúen con los escritorios virtuales, y desempeñan un papel fundamental en garantizar que la experiencia del escritorio virtual sea lo más fluida y eficiente posible.

También puedes activar la opción de **visor directo** que te proporcionará un enlace directo para conectarte a tu escritorio sin necesidad de autenticarte en el sistema.


### Tabla resumen sobre las características de los visores

<table>
  <thead>
    <tr>
      <th>IsardVDI Viewer</th>
      <th>SPICE Viewer</th>
      <th>Browser Viewer</th>
      <th>RDP Viewer</th>
      <th>RDP Browser viewer</th>
      <th>RDP VPN viewer</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>Protocol</strong></td>
      <td>SPICE</td>
      <td>VNC-SPICE</td>
      <td>RDP</td>
      <td>RDP</td>
      <td>RDP</td>
    </tr>
    <tr>
      <td><strong>HTML5 web client</strong></td>
      <td>-</td>
      <td>NoVNC</td>
      <td>-</td>
      <td>Guacamole</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Windows viewer</strong></td>
      <td>Remote-viewer<br><a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11 installer</a></td>
      <td>Web Browser</td>
      <td>Remote Desktop Viewer (windows)</td>
      <td>Web Browser</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Linux viewer</strong></td>
      <td>sudo apt install virt-viewer<br>sudo dnf install remote-viewer</td>
      <td>Web Browser</td>
      <td>Remmina</td>
      <td>Web Browser</td>
      <td>Remmina</td>
    </tr>
    <tr>
      <td><strong>Mac Viewer</strong></td>
      <td>You can follow the <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">installation guide</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
    </tr>
    <tr>
      <td><strong>Android Viewer</strong></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">Free version</a> on the Play Store and also a <a href="https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE">paid version</a> with more features.</td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Apple iOS Viewer</strong></td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">Paid version</a> on Apple Store</td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Download File Extension</strong></td>
      <td>*.vv</td>
      <td>Web Browser</td>
      <td>*.rdp</td>
      <td>-</td>
      <td>*.rdp</td>
    </tr>
    <tr>
      <td><strong>Copy / Paste real desktop to virtual desktop</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Wireguard client required to access user VPN</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>USB Redirection</strong></td>
      <td>✓ <br>(with windows you need to install usb)</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Audio</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Adapt desktop screen size to viewer</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>You can see how the desktop boots</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
    </tr>
    <tr>
      <td><strong>You must have IP address and RDP service on your virtual desktop</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
  </tbody>
</table>


### Tabla resumen sobre compatibilidad en sistemas operativos

<table>
  <thead>
    <tr>
      <th>VDI Viewer</th>
      <th>Supported OS</th>
      <th>Protocols</th>
      <th>Multimedia Redirection</th>
      <th>USB Redirection</th>
      <th>Video Compression</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Microsoft Remote Desktop</td>
      <td>Windows, Mac, iOS, Android</td>
      <td>RDP</td>
      <td>✓</td>
      <td>✓</td>
      <td>RemoteFX</td>
    </tr>
    <tr>
      <td>Spice</td>
      <td>Windows, Linux</td>
      <td>SPICE</td>
      <td>✓</td>
      <td>✓</td>
      <td>Lossless and Lossy</td>
    </tr>
    <tr>
      <td>VNC</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✗</td>
      <td>✓</td>
      <td>Tight and Zlib</td>
    </tr>
    <tr>
      <td>Guacamole HTML5 Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>RDP, VNC, SSH</td>
      <td>✓</td>
      <td>✓</td>
      <td>JPEG, PNG</td>
    </tr>
    <tr>
      <td>noVNC Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✓</td>
      <td>✓</td>
      <td>Tight, Hextile, CopyRect, Raw</td>
    </tr>
  </tbody>
</table>


### Diferencias entre visores (en el navegador y de cliente escritorio)

Se pueden diferenciar dos tipos principales de visores:

* **Visores integrados en el navegador**: el visor queda integrado dentro de una página web. Desde una pestaña en el navegador se pueden controlar los sistemas operativos de los escritorios. La decodificación de la señal de vídeo, y el envío de la señal del ratón y la del teclado se hace desde una pestaña del navegador. 

    * **Ventajas**: 
        * No se necesita tener un cliente instalado, funciona desde cualquier dispositivo (ordenadores, tablets, móviles) y sistema operativo que tenga un navegador.
    * **Inconvenientes**: 
        * La decodificación no es tan eficiente como en un visor dedicado, se puede percibir **más lentitud** en el refresco del escritorio y al trabajar moviendo elementos dentro del visor de éste.
        * No podemos redirigir al escritorio virtual dispositivos locales conectados por USB.

* **Aplicaciones cliente de escritorio**: son aplicaciones dedicadas a ejecutarse de visor, están optimizadas y son la mejor opción para tener la mejor experiencia de usuario.

    * **Ventajas**: 
        * Optimizadas para los protocolos cliente de escritorio, mejor rendimiento que en los visores integrados en el navegador. Si no existen problemas de red (latencia y ancho de banda adecuados) la sensación de trabajo con el escritorio es similar a la de un equipo real.
        * Aunque depende del tipo de protocolo y de la versión del cliente, por lo general, estos visores ofrecen opciones avanzadas tales como la redirección de dipostivos por puerto USB y arrastrar archivos y portapapeles desde el escritorio virtual a la máquina anfitriona y viceversa.
    * **Inconvenientes**: 
        * No en todos los casos los clientes vienen preinstalados con el sistema operativo, lo que conlleva a tener que realizar una instalación de software en el equipo anfitrión. 
        * Funciones avanzadas no disponibles en función de las versiones del cliente y la versión del protocolo


### Puertos y proxies

En IsardVDI se ha realizado un esfuerzo importante por no tener que abrir puertos extra y encapsular en proxies HTTP las conexiones. Se ha tratado de adaptar la plataforma a cualquier situación en la que exista un firewall de por medio que pueda bloquear o dificultar las conexiones. Por defecto se usan los puertos:

* TCP/**80** para el proxy donde se encapsulan las conexiones del protocolo **SPICE**
* TCP/**443** para la web y los visores integrados **en el navegador**
* TCP/**9999** para el proxy por donde se encapsulan las conexioes del protocolo **RDP**


## FAQs - Preguntas frecuentes

<details>
  <summary>No puedo escanear un dispositivo USB mediante visor RDP</summary>
  <p>Si el usuario tiene un anfitrión Windows e intenta escanear un dispostivio USB en un escritorio virtual sin éxito, se puede seguir <b><a href="../remote_fx.es">este manual sobre el uso de RemoteFX</a></b>.</p>
</details>
