# Mahaigaina editatu

Honako atal hauek edita daitezke, mahaigainaren izenaz, deskribapenaz eta irudi deskribatzaileaz aparte:

Mahaigaina editatzeko ikono honen bidez egin daiteke: ![](edit_desktop.es.images/edit_desktop3.png)

![](./edit_desktop.eu.images/edit_desktop4.png){width="90%"}


## Bisoreak

Atal honetan, mahaigainera **sartzeko** zer [bisore](../user/viewers/viewers.eu.md) erabili nahi diren aukera daiteke.
Mahaigaina pantaila osoan abiarazi nahi den ala ez ere aukera daiteke.

![](./create_desktop.eu.images/creacion5.png){width="90%"}


## RDP bidezko Logina

Atal honetan, **RDP bisorearen erabiltzailea eta pasahitza** esleitzen dira (mahaigaineko SEn sortutako erabiltzailea). Konfigurazio hori soilik beharrezkoa da mahaigainean RDPren bidez identifikatuta egon nahi ez bada eta RDP bisorea hautatuta badago


![](create_desktop.eu.images/login_rdp.png){width="60%"}


## Hardwarea

Atal honetan, mahaigainaren hardwarea edita daiteke.

- **Bideoak**: lehenetsita **beti da Default** aukera
- **Boot**: 
    - **Hard disk** mahaigainean sistema eragile bat instalatuta badago
    - **CD/DVD** **[media](../advanced/media.eu.md)** bat aukeratuta badago eta bertatik abiarazi nahi bada.
- **Disk bus**: lehenetsita beti aukeratzen da **Default aukera**
- **Sareak**: mahaigainera nahi diren sareen interfazeen zerrenda

![](create_desktop.eu.images/hardware.png){width="90%"}


## Erreserba daitezkeenak

Atal honetan, mahaigainera lotu nahi den **GPUko txartelaren profil bat** hauta daiteke (baliteke eskuragarri ez egotea).

![](create_desktop.eu.images/hardware_2.png){width="90%"}


## Media

Atal honetan, erabiltzailearen [media](../advanced/media.eu.md) bat gehi dakioke, edo erabiltzailearekin partekatuta dagoena.


![](create_desktop.eu.images/media.png){width="90%"}


## Irudia

Atal honetan, mahaigainak ikuspegi nagusian izango duen **azaleko irudia** aukera daiteke.

![](./edit_desktop.eu.images/edit_desktop10.png){width="90%"}