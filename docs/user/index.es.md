# Introducción

El manual de usuario se organiza siguiendo el orden en el menú web IsardVDI, y consiste en:

- Los métodos de **Inicio de sesión** disponibles para acceder al sistema.
- **Escritorios**, sus acciones y visores.
- **Plantillas** que permiten ser replicadas como múltiples escritorios.
- Se pueden subir **Mitjans**, normalmente una ISO para crear un escritorio nuevo.
- **Despliegues** permite supervisar los escritorios mientras trabajáis.
- Se necesitan **Reservas** para recursos como las vGPU.
- **Almacenamiento** muestra cuánto se utilizan vuestros discos de escritorio.
- **Administración** es un enlace que permite a los gestores y administradores hacer funciones avanzadas.
- **Perfil** no solo muestra vuestra información y permite restablecer la contraseña, también muestra vuestra cuota y uso.

![](index.images/global_view.png)