# Configuration

You can personalize many features and parameters to adapt your installation to your environment. This parameters are in **isardvdi.cfg.example** file in git repository that you copied to your custom *isardvdi.cfg* before doing `build.sh` and bringing project up again with `docker-compose up -d`.

For an `all-in-one` setup or just to test the system at least you should setup in this file:

* **DOMAIN**=localhost -> should be set to your server IP or DNS reacheable from other hosts (if not, viewers connecting from other computers won't reach your server and will try to connect to localhost). If you plan to use IsardVDI only from your computer then it is ok to let it as `localhost`.

* **WEBAPP_ADMIN_PWD**=IsardVDI -> of course, change it as many services will use this password, not only the admin user for isard. For example the `filebrowser` storage and `grafana` will be set to this password.

Also generate new keys using the command `openssl rand -base64 32`:
* **WEBAPP_SESSION_SECRET**=xq0Z3MP5ujxrQxtMGxgPiijH9xpuxkyP04R6At/V+g4=
* **API_ISARDVDI_SECRET**=kpWpdF0NtI2XCEfzMp36hdSV9S42E7axS8D5TvP9c0A=
* **INFLUXDB_ADMIN_TOKEN_SECRET**=9eFW/Qi29hL3hFGUP8wIGH89XKCH8s1k0il44GCRF2g=
* **API_HYPERVISORS_SECRET**=B5/bUEUzIC+AjNQRmFh3vxR3VeIKirwdeL/xuHPVO+E=

Optionally if your server has port 80 open to public Internet you can get *isard-portal* container to generate an renew LetsEncrypt certificates for you if you set your `DOMAIN` to your public DNS and `LETSENCRYPT_EMAIL` to your email.

There are many other parameters in this file that are mainly used when complex IsardVDI infrastructure is used. Do not modify them unless you know what you are doing (file has comments)
