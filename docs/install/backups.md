# Backups

## Backups from webapp

You can do manual backups, download, upload and restore full or individual parts of the tables in the system.

To do that you should got to administration and then in config you can operate backups.

Also, you can activate an automated scheduled cron task at any hour to do automatically the backup.

The backups can be found on disk at ```/opt/isard-local/backups``` or `BACKUP_DIR` environment variable.

## Backups from system

You can activate backups from system (preferred in production). You should dump the database and also de disks (usually at least ```/opt/isard/templates``` and recommended the full ```/opt/isard``` path).

To easy the backup process we have added a new container: isard-backupninja.

This will create backups from db and disks, using borg backup and backupninja. This allows you to setup in isardvdi.cfg.example the backup behaviour like this:

```
  ## Automated backups (https://0xacab.org/liberate/backupninja)
  #BACKUP_DIR=/opt/isard-local/backup
  #BACKUP_REPORT_EMAIL=root

  #BACKUP_NFS_ENABLED=false
  #BACKUP_NFS_SERVER=172.16.0.10
  #BACKUP_NFS_FOLDER=/remote/backupfolder

  #BACKUP_DB_ENABLED=false
  #BACKUP_DB_WHEN="everyday at 01"
  #BACKUP_DB_PRUNE="--keep-daily=1 --keep-weekly=24 --keep-monthly=12 --keep-within=14d --save-space"

  #BACKUP_DISKS_ENABLED=false
  #BACKUP_DISKS_WHEN="everyday at 01"
  #BACKUP_DISKS_PRUNE="--keep-daily=1 --keep-weekly=4 --keep-monthly=3 --keep-within=7d --save-space"
  #BACKUP_DISKS_TEMPLATES_ENABLED=false
  #BACKUP_DISKS_GROUPS_ENABLED=false
  #BACKUP_DISKS_MEDIA_ENABLED=false
```

The variables are self explanatory. By default no backup is activated and no isard-backupninja container started. If you activate any (database or disks) then the container will be build (./build.sh) and will come up when you execute ```docker-compose up -d``` again.

  WARNING: The ```BACKUP_DIR``` (or the default backup dir ```/opt/isard-local/backup```) will
  hold new ```db``` and ```disks``` folders that will be created the first time. If they exist, should be empty the first time as backupninja will initialize the first time with borg. If not, the backup will fail.

If you have an external NAS with NFS4 export (note: only nvs v4 supported) you can set up the ```BACKUP_NFS_...``` vars and it will be mounted the before executing the backup and unmounted at the end. You can check whether it works as the first time the container will create ```db``` and/or ```disks``` folders based on what you activated and also it will show in container logs the successfull NFS mount. The export at the server should allow de DOCKER_NET (172.31.255.X/24) and(or) the isard-backupninja host (.88) to access the exported path. ([Release 9.14.0](https://gitlab.com/isard/isardvdi/-/releases/v9.14.0))

Also we have included an script that will make the process of restoring backup, listing, checking integrity, etc...

- List disks|db backups:

```
# docker exec -ti isard-backupninja backup.sh list disks
2021-11-18T10:35:58
2021-11-18T10:37:05
2021-11-18T10:38:13
```
- Show repository info
```
# docker exec -ti isard-backupninja backup.sh info disks
Warning: Attempting to access a previously unknown unencrypted repository!
Do you want to continue? [yN] yes (from BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK)
Repository ID: 1f195947728add72fd71a5d633f66fd2a17948bc4d474225045ea879274ce709
Location: /backup/disks
Encrypted: No
Cache: /root/.cache/borg/1f195947728add72fd71a5d633f66fd2a17948bc4d474225045ea879274ce709
Security dir: /root/.config/borg/security/1f195947728add72fd71a5d633f66fd2a17948bc4d474225045ea879274ce709
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
All archives:                2.20 GB              1.65 GB            330.91 MB

                       Unique chunks         Total chunks
Chunk index:                     169                  830
```

- Check backup integrity:

```
# docker exec -ti isard-backupninja backup.sh check-integrity disks 2021-11-18T10:55:58
opt/isard/templates
opt/isard/groups
opt/isard/groups/default
opt/isard/groups/default/default
opt/isard/groups/default/default/local
opt/isard/groups/default/default/local/admin-admin
opt/isard/groups/default/default/local/admin-admin/downloaded_zxspectrum.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_tetros.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_slax93.qcow2
```

- Only list files:

```
# docker exec -ti isard-backupninja backup.sh show-files disks 2021-11-18T10:55:58
opt/isard/templates
opt/isard/groups
opt/isard/groups/default
opt/isard/groups/default/default
opt/isard/groups/default/default/local
opt/isard/groups/default/default/local/admin-admin
opt/isard/groups/default/default/local/admin-admin/downloaded_zxspectrum.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_tetros.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_slax93.qcow2
```

- Extract all files

```
# docker exec -ti isard-backupninja backup.sh extract disks 2021-11-18T10:55:58
opt/isard/templates
opt/isard/groups
opt/isard/groups/default
opt/isard/groups/default/default
opt/isard/groups/default/default/local
opt/isard/groups/default/default/local/admin-admin
opt/isard/groups/default/default/local/admin-admin/downloaded_zxspectrum.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_tetros.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_slax93.qcow2

# tree /opt/isard-local/backup/extract/
/opt/isard-local/backup/extract/
└── opt
    └── isard
        ├── groups
        │   └── default
        │       └── default
        │           └── local
        │               └── admin-admin
        │                   ├── downloaded_slax93.qcow2
        │                   ├── downloaded_tetros.qcow2
        │                   └── downloaded_zxspectrum.qcow2
        └── templates
```

- Extract one file only

```
# rm -rf /opt/isard-local/backup/extract/*

# docker exec -ti isard-backupninja backup.sh extract disks 2021-11-18T10:55:58 opt/isard/groups/default/default/local/admin-admin/downloaded_slax93.qcow2
opt/isard/groups/default/default/local/admin-admin/downloaded_slax93.qcow2

# tree /opt/isard-local/backup/extract/
/opt/isard-local/backup/extract/
└── opt
    └── isard
        └── groups
            └── default
                └── default
                    └── local
                        └── admin-admin
                            └── downloaded_slax93.qcow2
```

- Do a manual backup:

```
# docker exec -ti isard-backupninja backup.sh execute-now disks
------------------------------------------------------------------------------
Archive name: 2021-11-19T09:33:39
Archive fingerprint: 3c0b9403e34830b2baca706bc54ad5e663b5d621f96087ce018fe47b0a77adb7
Time (start): Fri, 2021-11-19 09:33:39
Time (end):   Fri, 2021-11-19 09:33:42
Duration: 2.58 seconds
Number of files: 8
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:              440.85 MB            330.91 MB                510 B
All archives:                2.65 GB              1.99 GB            330.91 MB

                       Unique chunks         Total chunks
Chunk index:                     170                  996
------------------------------------------------------------------------------
```

- Check nfs mount ([Release 9.14.0](https://gitlab.com/isard/isardvdi/-/releases/v9.14.0))

```
# docker exec -ti isard-backupninja backup.sh check-nfs-mount
BACKUP NFS FOLDER MOUNTED:
192.168.0.200:/storage /backup nfs4 rw,relatime,vers=4.1,rsize=32768,wsize=32768,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=172.31.255.88,local_lock=none,addr=192.168.0.200 0 0
BACKUP NFS UnMOUNTED

```
