# Requirements

You only need to have **docker** service and **docker compose** installed. Example with Ubuntu 22 (docker compose now it is integrated in docker).

- Install **Docker**: https://docs.docker.com/engine/installation/

    - Note: docker 17.04 or newer needed for docker-compose.yml v3.2


```bash
sudo apt-get remove docker docker-engine docker.io containerd runc curl
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo systemctl status docker
```

**NOTE**: Your hardware needs to have virtualization enabled. You can check that in your BIOS but also from CLI:

```
egrep ‘(vmx|svm)’ /proc/cpuinfo
```
​	If you see nothing in the output your CPU has no virtualization capabilites or they are disabled in BIOS.


In old distros **docker** version could be older and you'll need to install also **docker-compose**:

- Install **docker-compose**: https://docs.docker.com/compose/install/

    - Note: docker-compose 1.12 or newer needed for docker-compose.yml v3.5. You can install last version using pip3:

```bash
apt install python3-pip -y
pip3 install docker-compose
```