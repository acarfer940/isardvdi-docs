# Actualización

## Desde Inicio rápido

### Manualmente

**Recomendamos revisar la nueva versión de isardvdi.cfg.example por si hay nuevos parámetros que quieras configurar.**

Cada vez que se realiza una actualización hay que correr el script build.sh que leerá las variables de configuración de isardvdi.cfg. Después de hacer un build.sh descargamos la versión actualizada de los contenedores y rearrancamos los que hayan sido modificados, IsardVDI debería de volver a estar operativo. 

```
cd path_to_isard_source
git pull
./build.sh
docker-compose pull
docker-compose up -d
```

### Automático

Recomendamos configurar actualizaciones automáticas mediante un cron que ejecute el siguiente [script](https://gitlab.com/isard/isardvdi/-/blob/main/sysadm/isard-upgrade-cron.sh).

**En caso de modificaciones en la versión de isardvdi.cfg.example no se realizará la actualización automática.**