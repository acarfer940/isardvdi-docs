# Guia d'instal·lació Windows Server 2022 i 2019

## Pre-instal·lació

1. Es descarrega la imatge ISO de Windows Server [2019](https://www.microsoft.com/es-es/evalcenter/download-windows-server-2019) o [2022](https://www.microsoft.com/es-es/evalcenter/download-windows-server-2022)

2. Es [crea un escriptori a partir d'aquest mitjà](../../../advanced/media.ca/#crear-nou-escriptori-a-partir-de-mitja) amb el següent hardware virtual:

    - Visors: **SPICE**
    - vCPUS: **4**
    - Memòria (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Mida del disc (GB): **100**
    - Xarxes: **Default**
    - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

3. S'[edita l'escriptori i s'assignen més mitjans](../../../user/edit_desktop.ca/#media), de tal forma que acabi tenint els següents:

    - **WinServer2022 - ES** (instal·lador)
    - **virtio-win-X** (controladors)
    - **Optimization Tools** (programari d'optimizació per a S.O. Windows)


## Instal·lació

1. **S'envia "Ctr+Alt+Supr"** i es polsa qualsevol tecla del teclat a la segona pantalla

    ![](install.es.images/pantalla1.png){width="45%"}
    ![](install.es.images/send_key.png){width="13%"}
    ![](install.es.images/pantalla2.png){width="45%"}

2. Tipus d'instal·lació

    ![](install.es.images/1-es.png){width="45%"}
    ![](install.es.images/2-es.png){width="45%"}

    ![](install.es.images/3-es.png){width="45%"}
    ![](install.es.images/4-es.png){width="45%"}

3. Carregar controladors de sistema operatiu

    ![](install.es.images/5-es.png){width="45%"}
    ![](install.es.images/6-es.png){width="45%"}

    ![](install.es.images/7-es.png){width="45%"}
    ![](install.es.images/8-es.png){width="45%"}

    ![](install.es.images/9-es.png){width="45%"}
    ![](install.es.images/10-es.png){width="45%"}

    ![](install.es.images/11-es.png){width="45%"}
    ![](install.es.images/12-es.png){width="45%"}


## Configuració

1. S'apaga i [edita l'escriptori](../../../user/edit_desktop.ca) amb el següent hardware virtual:

    - Visors: **RDP** i **RDP al navegador**
    - Login RDP:
        - Usuari: **Administrador**
        - Contrasenya: **Aneto_3404**
    - vCPUS: **4**
    - Memòria (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Xarxes: **Default** i **Wireguard VPN**

2. S'arrenca l'escriptori, trigarà uns segons en iniciar el sistema operatiu

    ![](install.es.images/13-es.png){width="45%"}

3. S'assigna la contrasenya **Aneto_3404** a l'usuari **Administrador**

    ![](install.es.images/14-es.png){width="45%"}
    ![](install.es.images/15-es.png){width="45%"}

4. S'instal·len els dos controladors **virtio** del mitjà de l'escriptori. Ambdues instal·lacions són ràpidas i només s'ha de fer clic a **Next**

    ![](install.es.images/16-es.png){width="45%"}
    ![](install.es.images/17-es.png){width="45%"}

    ![](install.es.images/18-es.png){width="45%"}
    ![](install.es.images/19-es.png){width="45%"}

5. S'habiliten les connexions per **Escriptori Remot**. S'habilita la primera casella, i es deshabilita la segona casella, en **Configuración avanzada**

    ![](install.es.images/20-es.png){width="20%"}
    ![](install.es.images/21-es.png){width="38%"}
    ![](install.es.images/22-es.png){width="39%"}

6. Es comproven les **actualitzacions del sistema**, les cuals trigaran bastant en descarregar-se i instal·lar-se, per estar al dia amb la última versió de Windows Server (segurament es necessiti reiniciar el sistema múltiples vegades en la recerca de noves actualitzacions).

    ![](install.es.images/23-es.png){width="60%"}


## Optimization tools

1. S'obre l'executable del mitjà assignat a l'escriptori

    ![](install.es.images/24-es.png){width="45%"}

2. Es fa clic al botó **Analizar** i seguidament a **Opciones comunes**

    ![](install.es.images/25-es.png){width="45%"}
    ![](install.es.images/26-es.png){width="45%"}

3. S'assignen les opcions com a les següents imatges

    ![](install.es.images/27-es.png){width="45%"}
    ![](install.es.images/28-es.png){width="45%"}

    ![](install.es.images/29-es.png){width="45%"}
    ![](install.es.images/30-es.png){width="45%"}

    ![](install.es.images/31-es.png){width="45%"}
    ![](install.es.images/32-es.png){width="45%"}

    ![](install.es.images/33-es.png){width="45%"}
    ![](install.es.images/34-es.png){width="45%"}

4. (**SI L'IDIOMA CATALÀ ESTÀ DEFINIT, SI NO, OMITIR**) Es filtra per la paraula **idioma** i es desactiven les següents 3 opcions:

    ![](../win10/install.es.images/58.png){width="45%"}

5. Es fa clic al botó **Optimizar** i s'espera què retorni una pantalla com la segona amb el resum del resultat

    ![](install.es.images/36-es.png){width="45%"}

6. Es reinicia el sistema
