# Debian Server 12 install

## Instal·lació del SO

![](installation.images/6.png){width="25%"}
![](installation.images/9.png){width="65%"}

**No seleccionar cap denv**

![](installation.images/24.png){width="70%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

**Comandaments bàsic**
```
$ su -
$ apt install sudo
$ visudo 
#add this at the end of file
isard   ALL=(ALL:ALL)   ALL
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```