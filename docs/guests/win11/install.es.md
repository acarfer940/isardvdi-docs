# Guía de instalación Windows 11

Para hacer la instalación de Windows 11, se tiene que:

1. Ir a la página web de Microsoft y descargar la iso de [Windows 11](https://www.microsoft.com/es-es/software-download/windows11) versión 64 bits en el idioma que se prefiera.
2. Se descarga la última versión estable de [drivers de virtio](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/)


## Pre-instalación

1. Se [crea un escritorio de el media subido](../../../advanced/media.es/#generar-escritorio-a-partir-de-media) con el siguiente Hardware:

    !!! tip "Recomendado"
        - Visores: **SPICE**/**VNC**
        - vCPUS: **4**
        - Memoria (GB): **6**
        - Videos: **Default**
        - Boot: **CD/DVD**
        - Disk Bus: **Default**
        - Tamaño del disco (GB): **140**
        - Redes: **Default**
        - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

2. Se [edita el escritorio y se le añaden estos medios](../../../user/edit_desktop.es/#media):

    - **Win11_22H2_ES** (instalador)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (software de optimización para S.O. Windows)

!!! warning "Importante"
    **Si se quiere tener red durante el proceso de instalación, se tiene que seleccionar la red "Intel1000"**

### Edición XML

Es importante editar el xml del escritorio ya que sino la instalación no se hará. Para poder editar el xml, se tiene que tener un [**rol "manager" o "admin".**](../../../manager/index.es/#roles)

![](install.es.images/1.png){width="45%"}


#### OS

En el apartado de "os" dentro del xml se tiene que modificar y cambiar de "pc-i440fx-2.8" a "q35" y añadir unos elementos:

```
<os>
    <boot dev="hd"/>
    <type arch="x86_64" machine="q35">hvm</type>
    <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
</os>
```

#### Hyperv

En el apartado de <hyperv> se tiene que tener esto ya que Windows 11 lo recomienda para un mejor rendimiento:

```
<hyperv>
    <relaxed state="on"/>
    <vapic state="on"/>
    <spinlocks state="on" retries="8191"/>
    <synic state="on"/>
    <stimer state="on"/>
    <vpindex state="on"/>
    <tlbflush state="on"/>
    <ipi state="on"/>
</hyperv>
    
```

#### TMP
    
Es un requisito de Windows 11 y se debe añadir:
    
```
<tpm model="tpm-tis">
    <backend type="emulator" version="2.0"/>
</tpm>
```

#### Audio
    
Forzamos que el audio salga por Spice con:

```
<sound model="ich9">
    <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
</sound>
<audio id="1" type="spice"/>
```

#### Channel

```
<channel type="spicevmc">
    <target type="virtio" name="com.redhat.spice.0"/>
    <address type="virtio-serial" controller="0" bus="0" port="1"/>
</channel>
```

#### Devices PCIe + USB

```
<controller type="usb" index="0" model="qemu-xhci" ports="15">
    <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
</controller>
    <controller type="pci" index="0" model="pcie-root"/>
    <controller type="pci" index="1" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="1" port="0x10"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="2" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="2" port="0x11"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
    </controller>
    <controller type="pci" index="3" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="3" port="0x12"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
    </controller>
    <controller type="pci" index="4" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="4" port="0x13"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
    </controller>
    <controller type="pci" index="5" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="5" port="0x14"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
    </controller>
    <controller type="pci" index="6" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="6" port="0x15"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
    </controller>
    <controller type="pci" index="7" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="7" port="0x16"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
    </controller>
    <controller type="pci" index="8" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="8" port="0x17"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
    </controller>
    <controller type="pci" index="9" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="9" port="0x18"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
    </controller>
    <controller type="pci" index="10" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="10" port="0x19"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
    </controller>
    <controller type="pci" index="11" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="11" port="0x1a"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
    </controller>
    <controller type="pci" index="12" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="12" port="0x1b"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
    </controller>
    <controller type="pci" index="13" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="13" port="0x1c"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
    </controller>
    <controller type="pci" index="14" model="pcie-root-port">
      <model name="pcie-root-port"/>
      <target chassis="14" port="0x1d"/>
      <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
    </controller>
    <controller type="sata" index="0">
      <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
    </controller>
    <controller type="virtio-serial" index="0">
      <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
    </controller>
    <serial type="pty">
      <target type="isa-serial" port="0">
        <model name="isa-serial"/>
      </target>
    </serial>
```

#### Memballoon
    
```
<memballoon model="virtio">
    <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
</memballoon>
```
    
#### Console
    
```
 <console type="pty">
    <target type="serial" port="0"/>
</console>
```


<details>
  <summary>Ejemplo de XML para Windows 11</summary>
  <p><b>Los "#" representan ids, cada id es diferente</b></p>
```
    <domain type="kvm">
    <name>#########</name>
    <uuid>#########</uuid>
    <metadata>
        <libosinfo:libosinfo xmlns:libosinfo="http://libosinfo.org/xmlns/libvirt/domain/1.0">
        <libosinfo:os id="http://microsoft.com/win/10"/>
        </libosinfo:libosinfo>
    </metadata>
    <memory unit="KiB">######</memory>
    <currentMemory unit="KiB">######</currentMemory>
    <vcpu placement="static">#</vcpu>
    <os>
        <boot dev="hd"/>
        <type arch="x86_64" machine="q35">hvm</type>
        <loader readonly="yes" secure="no" type="pflash">/usr/share/OVMF/OVMF_CODE.fd</loader>
    </os>
    <features>
        <acpi/>
        <apic/>
        <hyperv>
        <relaxed state="on"/>
        <vapic state="on"/>
        <spinlocks state="on" retries="8191"/>
        <synic state="on"/>
        <stimer state="on"/>
        <vpindex state="on"/>
        <tlbflush state="on"/>
        <ipi state="on"/>
        </hyperv>
        <vmport state="off"/>
    </features>
    <cpu mode="host-model">
        <topology sockets="1" dies="1" cores="4" threads="1"/>
    </cpu>
    <clock offset="localtime">
        <timer name="rtc" tickpolicy="catchup"/>
        <timer name="pit" tickpolicy="delay"/>
        <timer name="hpet" present="no"/>
        <timer name="hypervclock" present="yes"/>
    </clock>
    <pm>
        <suspend-to-mem enabled="no"/>
        <suspend-to-disk enabled="no"/>
    </pm>
    <devices>
        <emulator>/usr/bin/qemu-kvm</emulator>
        <disk type="file" device="disk">
        <driver name="qemu" type="qcow2"/>
        <source file="/isard/groups/##############.qcow2"/>
        <target dev="vda" bus="virtio"/>
        </disk>
        <disk type="file" device="cdrom">
        <driver name="qemu" type="raw"/>
        <source file="/isard/media/###########.iso"/>
        <target dev="sda" bus="sata"/>
        <readonly/>
        </disk>
        <controller type="usb" index="0" model="qemu-xhci" ports="15">
        <address type="pci" domain="0x0000" bus="0x02" slot="0x00" function="0x0"/>
        </controller>
        <controller type="pci" index="0" model="pcie-root"/>
        <controller type="pci" index="1" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="1" port="0x10"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="2" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="2" port="0x11"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x1"/>
        </controller>
        <controller type="pci" index="3" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="3" port="0x12"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x2"/>
        </controller>
        <controller type="pci" index="4" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="4" port="0x13"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x3"/>
        </controller>
        <controller type="pci" index="5" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="5" port="0x14"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x4"/>
        </controller>
        <controller type="pci" index="6" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="6" port="0x15"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x5"/>
        </controller>
        <controller type="pci" index="7" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="7" port="0x16"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x6"/>
        </controller>
        <controller type="pci" index="8" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="8" port="0x17"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x7"/>
        </controller>
        <controller type="pci" index="9" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="9" port="0x18"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x0" multifunction="on"/>
        </controller>
        <controller type="pci" index="10" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="10" port="0x19"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x1"/>
        </controller>
        <controller type="pci" index="11" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="11" port="0x1a"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x2"/>
        </controller>
        <controller type="pci" index="12" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="12" port="0x1b"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x3"/>
        </controller>
        <controller type="pci" index="13" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="13" port="0x1c"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x4"/>
        </controller>
        <controller type="pci" index="14" model="pcie-root-port">
        <model name="pcie-root-port"/>
        <target chassis="14" port="0x1d"/>
        <address type="pci" domain="0x0000" bus="0x00" slot="0x03" function="0x5"/>
        </controller>
        <controller type="sata" index="0">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1f" function="0x2"/>
        </controller>
        <controller type="virtio-serial" index="0">
        <address type="pci" domain="0x0000" bus="0x03" slot="0x00" function="0x0"/>
        </controller>
        <serial type="pty">
        <target type="isa-serial" port="0">
            <model name="isa-serial"/>
        </target>
        </serial>
        <console type="pty">
        <target type="serial" port="0"/>
        </console>
        <channel type="spicevmc">
        <target type="virtio" name="com.redhat.spice.0"/>
        <address type="virtio-serial" controller="0" bus="0" port="1"/>
        </channel>
        <interface type="network">
        <source network="default"/>
        <mac address="52:54:00:43:ba:0b"/>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <interface type="bridge">
        <source bridge="ovsbr0"/>
        <mac address="52:54:00:0f:0f:a5"/>
        <virtualport type="openvswitch"/>
        <vlan>
            <tag id="4095"/>
        </vlan>
        <model type="virtio"/>
        <bandwidth/>
        </interface>
        <input type="tablet" bus="usb"/>
        <graphics type="spice" port="-1" tlsPort="-1" autoport="yes">
        <listen type="address" address="0.0.0.0"/>
        <image compression="auto_glz"/>
        <jpeg compression="always"/>
        <zlib compression="always"/>
        <playback compression="off"/>
        <streaming mode="all"/>
        </graphics>
        <sound model="ich9">
        <address type="pci" domain="0x0000" bus="0x00" slot="0x1b" function="0x0"/>
        </sound>
        <audio id="1" type="spice"/>
        <video>
        <model type="qxl" ram="65536" vram="65536" vgamem="16384" heads="1" primary="yes"/>
        <alias name="video0"/>
        </video>
        <tpm model="tpm-tis">
        <backend type="emulator" version="2.0"/>
        </tpm>
        <redirdev bus="usb" type="spicevmc"/>
        <redirdev bus="usb" type="spicevmc"/>
        <memballoon model="virtio">
        <address type="pci" domain="0x0000" bus="0x04" slot="0x00" function="0x0"/>
        </memballoon>
    </devices>
    </domain>
```
</details>


## Instalación

### Windows 11

Abrimos el escritorio con Spice o VNC en navegador:

!!! Example "Visores"

    - Si se escoge **Spice**, cuando aparezca esta pantalla se tiene que ir a "Send key" y pulsar ++ctrl+alt+del++
    - Si se escoge **VNC en navegador**, se pulsa el botón de arriba a la derecha "Send ++ctrl+alt+del++ "

    === "Spice"
        ![](install.es.images/2.png){width="45%"}
        ![](install.es.images/3.png){width="45%"}

    === "VNC en navegador"
        ![](install.es.images/4.png)


Cuando se pulsa el botón, aparecerá este mensaje:

![](install.es.images/5.png){width="45%"}

En ese momento se pulsa dentro de la consola cualquier tecla, por ejemplo ++enter++ y se fuerza que arranque el instalador de windows 11

Aparece el boot de efi y la carga del instalador de windows:

![](install.es.images/6.png){width="45%"}

1. Tipo de instalación

    ![](install.es.images/7.png){width="45%"}
    ![](install.es.images/8.png){width="45%"}
    ![](install.es.images/9.png){width="45%"}
    ![](install.es.images/10.png){width="45%"}
    ![](install.es.images/11.png){width="45%"}

2. Cargar drivers de sistema operativo

    ![](install.es.images/12.png){width="45%"}
    ![](install.es.images/13.png){width="45%"}
    ![](install.es.images/14.png){width="45%"}
    ![](install.es.images/15.png){width="45%"}
    ![](install.es.images/16.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

### Sistema

1. Se apaga y se [edita el escritorio](../../../user/edit_desktop.es/#hardware), cambiando así el modo **"Boot"** y se selecciona "Hard disk". 

    ![](install.es.images/18.png){width="45%"}

2. Se vuelve a arrancar el escritorio, puede tardar unos minutos en arrancar el sistema.

    ![](install.es.images/19.png){width="45%"}

3. Se selecciona las siguientes opciones, puede que no se redimensione bien la pantalla en este momento y se tendrá que mover el contenido hacia arriba o hacia abajo

    ![](install.es.images/20.png){width="45%"}
    ![](install.es.images/21.png){width="45%"}
    ![](install.es.images/22.png){width="45%"}
    ![](install.es.images/23.png){width="45%"}
    ![](install.es.images/24.png){width="45%"}
    ![](install.es.images/25.png){width="45%"}
    ![](install.es.images/26.png){width="45%"}
    ![](install.es.images/27.png){width="45%"}
    ![](install.es.images/28.png){width="45%"}
    ![](install.es.images/29.png){width="45%"}
    ![](install.es.images/30.png){width="45%"}
    ![](install.es.images/31.png){width="45%"}
    ![](install.es.images/32.png){width="45%"}
    ![](install.es.images/33.png){width="45%"}
    ![](install.es.images/34.png){width="45%"}
    ![](install.es.images/35.png){width="45%"}

4. Se apaga el escritorio, se quita la imagen de instalación de Windows 11 y se le deja este media:

    - virtio-win-x (controladores)
    - Optimization Tools (programa de optimización para S.O Windows)


## Configuración

### Actualizar e instalar

1.  Se instalan los dos controladores virtio del media del escritorio (virtio-win-x). 

    ![](install.es.images/36.png){width="45%"}
    ![](install.es.images/37.png){width="45%"}
    ![](install.es.images/38.png){width="45%"}
    ![](install.es.images/39.png){width="45%"}
    ![](install.es.images/40.png){width="45%"}
    ![](install.es.images/41.png){width="45%"}
    ![](install.es.images/42.png){width="45%"}
    ![](install.es.images/43.png){width="45%"}
    ![](install.es.images/44.png){width="45%"}
    ![](install.es.images/45.png){width="45%"}

2.  Se comprueba si hay actualizaciones del sistema y se descargan para estar al día con la última versión.

    ![](install.es.images/46.png){width="45%"}

3.  Se instalan los siguientes programas y se guardan sus instaladores en una nueva carpeta que llamaremos **admin** en la ruta **C:\admin**

    !!! info inline "Programas"
        - Firefox
        - Google chrome
        - Libre Office
        - Gimp
        - Inkscape
        - LibreCAD
        - Geany
        - Adobe Acrobat Reader

    ![](install.es.images/47.png){width="45%"}

### Usuario admin y cambio de permisos

Se abre una **Powershell** con permisos de administrador:

![](install.es.images/49.png){width="45%"}


1. Se crea un usuario **admin** en el grupo **Administradores**

    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administradores" -Member "admin"
    ```

    ![](install.es.images/50.png){width="45%"}

2. Se crea un usuario **user** en el grupo **Usuarios**

    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Usuarios" -Member "user"
    ```
    ![](install.es.images/51.png){width="45%"}

3. Se le cambian los permisos a la carpeta **C:\admin**, botón derecho en la carpeta y seleccionar **"Propiedades"**. Se tiene que **deshabilitar la herencia** de la carpeta y borrar todos los usuarios que no son **Administradores**

    ![](install.es.images/52.png){width="45%"}
    ![](install.es.images/53.png){width="45%"}
    ![](install.es.images/54.png){width="45%"}
    ![](install.es.images/55.png){width="45%"}
    ![](install.es.images/56.png){width="45%"}

### Desinstalar aplicaciones y modificar configuraciones de Microsoft

1. Se abre un **Powershell** con permisos de administrador, y se desinstalan los siguientes paquetes y programas:

    ![](install.es.images/57.png){width="45%"}

    !!! Warning "Información importante"
        La desinstalación de estos programas conlleva a que el Windows vaya más rápido y tenga más espacio. Es importante destacar que también se eliminará la **Microsoft Store**. Si no se quiere eliminar, se tiene que obviar el comando: 
        ```
        Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
        ```
    <details>
        <summary>Comandos para la desinstalación de los programas en Powershell</summary>
        <p></p>
    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```
    </details>

2. Se abre **msconfig** mediante las teclas ++windows+r++ o desde una CMD, y se desactivan los servicios siguientes (se reinicia el equipo si lo pide):

    !!! info inline "Servicios"
        <font size="2">
            <ul>
            <li> Administrador de autenticación Xbox Live</li>
            <li> Centro de seguridad</li>
            <li> Firewall de Windows Defender</li>
            <li> Mozilla Maintenance Service</li>
            <li> Servicio de antivirus de Microsoft Defender</li>
            <li> Servicio de Windows Update Medic</li>
            <li> Windows Update</li>
            <li> Adobe Acrobat Update Service</li>
            <li> Servicio de Google Update (gupdate)</li>
            <li> Servicio de Google Update (gupdatem)</li>
            <li> Google Chrome Elevation Service</li>
            </ul>
        </font>

    ![](install.es.images/58.png){width="70%"}



3. Se abre el **Administrador de tareas** y en **aplicaciones de arranque** se deshabilita:

    !!! info inline "Programas"
        - Cortana
        - Teams
        - Microsoft OneDrive
        - Windows Security notifications

    ![](install.es.images/59.png){width="50%"}

4. Se desactivan las notificaciones de Windows en **Configuración** --> **Sistema** --> **Notificaciones y acciones**

    ![](install.es.images/60.png){width="45%"}

5. Se habilitan las conexiones por **Escritorio Remoto**. Se habilita la primera casilla, y se deshabilita la segunda casilla en **Configuración avanzada**

    ![](install.es.images/61.png){width="45%"}
    ![](install.es.images/62.png){width="45%"}

6. Para una mejor visibilidad del puntero del ratón, es recomendable seleccionar el modo invertido:

    ![](install.es.images/76.png){width="45%"}

7. Se desactiva estos elementos de la barra de tareas: 

    ![](install.es.images/77.png){width="45%"}

8. Se apaga y [edita el escritorio](../../../user/edit_desktop.es) con el siguiente hardware virtual:

    !!! tip "Recomendado"
        - Visores: **RDP** y **RDP en el navegador**
        - Login RDP:
            - Usuario: **isard**
            - Contraseña: **pirineus**
        - vCPUS: **4**
        - Memoria (GB): **8**
        - Videos: **Default**
        - Boot: **Hard Disk**
        - Disk Bus: **Default**
        - Redes: **Default** y **Wireguard VPN**


### Autologon

1. Para habilitar el inicio de sesión automático, se instala el **[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)**.

    !!! Info
        Si ya se añadió la iso del autlogon en el apartado de media en el escritorio no hace falta descargarlo.

2. Se descomprime el archivo descargado y se ejecuta **Autologon64**. Se escribe la contraseña **pirineus** (o la que se prefiera).

    ![](install.es.images/63.png){width="45%"}


## Optimization tools

1. Se pulsa el botón **Analizar**, seguidamente **Opciones comunes** y se configuran las opciones como en las siguientes imágenes.

    ![](install.es.images/64.png){width="45%"}
    ![](install.es.images/65.png){width="45%"}
    ![](install.es.images/66.png){width="45%"}
    ![](install.es.images/67.png){width="45%"}
    ![](install.es.images/68.png){width="45%"}
    ![](install.es.images/69.png){width="45%"}
    ![](install.es.images/70.png){width="45%"}
    ![](install.es.images/71.png){width="45%"}

    !!! warning "Importante"
        Si en el punto 1 de [la desinstalación de aplicaciones](../../../install.es/#desinstalar-aplicaciones-y-modificar-configuraciones-de-microsoft) no se ha borrado la Microsoft Store, se tiene que desmarcar la casilla de **"Aplicaciones de Store"**

        ![](install.es.images/72.png){width="45%"}

2. Se exporta el archivo resultante en la carpeta **C:\admin**

    ![](install.es.images/73.png){width="45%"}
    ![](install.es.images/74.png){width="45%"}
    ![](install.es.images/75.png){width="45%"}