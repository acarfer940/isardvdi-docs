# Configuración

!!! info "Roles con acceso"

    Solo los **administradores** tienen acceso a esta característica.

Para ir a la sección de "Config", se pulsa el botón ![](./users.es.images/users1.png)

![](./users.es.images/users2.png)

Y se pulsa el botón ![](./config.images/config1.png)

![](./config.images/config2.png)


## Programador de tareas

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Modo mantenimiento

El modo de mantenimiento desactiva las interfaces web para los usuarios con roles de manager, advanced y de user.

El administrador del sistema puede crear un archivo llamado `/opt/isard/config/maintenance` para cambiar al modo de mantenimiento al inicio. El archivo se elimina una vez que el sistema cambia al modo de mantenimiento.

El modo de mantenimiento se puede habilitar y deshabilitar a través de la sección de [configuración](./config.es.md) del panel de administración por parte de un usuario con función de administrador.

Para habilitar y deshabilitar esta función, se marca la casilla

![](./config.images/config6.png)

Una vez marcada la casilla, cuando los usuarios intenten iniciar sesión a su cuenta, les aparecerá un mensaje de mantenimiento

![](./config.images/config7.png)

![](./config.images/config8-es.png)
