# Quotes i Límits

## Quotes

Les quotes defineixen la quantitat de recursos que es poden utilitzar **individualment** (per cada usuari), en funció de la categoria i el grup al qual pertany l'usuari.

Les restriccions de quotes es divideixen de la manera següent:

![](./quotas_limits.images/quotas_limits8.png)

**Quota de creació**

* **Desktops**: Quantitat d'escriptoris que l'usuari pot crear.
* **Templates**: Quantitat de plantilles que l'usuari pot crear.
* **Media**: Quantitat de mitjans que l'usuari pot pujar o descarregar.

!!! Warning "Avís"
    * Els **recursos compartits** amb l'usuari **no es calculen a la quota d'usuari**.
    * Els escriptoris creats **a través dels desplegaments no es calculen a la quota d'usuari**.

**Quota d'escriptoris en execució**

* **Concurrent**: Quantitat total d'escriptoris que es poden iniciar alhora.
* **vCPUs**: Quantitat total de vCPUS dels escriptoris iniciats que es poden utilitzar alhora.
* **Memory (GB)**: Quantitat total de memòria vRAM dels escriptoris iniciats que es poden utilitzar alhora.

!!! Warning "Avís"
    * Quan s'inicia un **escriptori des d'un desplegament, es considera la quota de l'usuari**. Per tant, si l'usuari no té prou quota per iniciar l'escriptori, es mostrarà un error, fins i tot si el propietari del desplegament té prou quota. La lògica darrere d'aquest comportament és la propietat de l'escriptori, el propietari de l'escriptori és l'usuari, tot i que va ser creat pel propietari del desplegament.

**Quota de mida**

* **Disk size**: Mida màxima permesa en crear un escriptori des d'un mitjà.
* **Total size**: Mida total que es pot utilitzar considerant escriptoris, plantilles i mitjans.
* **Soft size**: Un cop assolit aquesta mida, s'avisarà a l'usuari.

Les quotes es poden establir per usuari, grup o categoria. S'apliquen per nivells, començant de baix a dalt (s'aplicarà la quota de nivell inferior). Per exemple, considerant l'estructura següent:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Category --> Group2[Group 2]
    Category --> Group3[Group 3]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
    Group2 --> User4[User 4]
    Group2 --> User5[User 5]
    Group2 --> User6[User 6]
    Group3 --> User7[User 7]
    Group3 --> User8[User 8]
    Group3 --> User9[User 9]
```

* Si l'usuari té una **quota d'usuari establerta** aquesta serà aplicada. Les quotes de **grup i categoria** seran **ignorades**.
* Si l'usuari **no té una quota d'usuari establerta** i s'ha establert una **quota de grup**, s'aplicarà la quota de grup. La **quota de categoria s'ignorarà**.
* Si no son establertes **ni la quota d'usuari ni la de grup s'aplicarà la quota de categoria**. La quota de categoria només es pot establir amb un [rol d'administrador](../admin/index.md).
* Si **no s'aplica cap quota**, tots els usuaris tindran accés a crear escriptoris, plantilles i isos **il·limitades** amb vCPU, vRAM de memòria i mida de disc **il·limitades**.

!!! Tip "Suggeriment"

    La restricció aplicada (quota d'usuari, grup o de categoria) i l'ús dels recursos es poden consultar a través de la [pàgina de perfil](../user/profile.md).

    === "Quota d'usuari aplicada"
        ![](./quotas_limits.images/quotas_limits5.png)

    === "Quota de grup aplicada"
        ![](./quotas_limits.images/quotas_limits6.png)

    === "Quota de categoria aplicada"
        ![](./quotas_limits.images/quotas_limits7.png)

## Límits

Els límits defineixen la quantitat de recursos que es poden utilitzar globalment en una categoria o un grup.

Per exemple, considerant un Grup 1 amb un límit de 10 escriptoris sense cap quota aplicada:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
```

La quantitat total d'escriptoris que es poden crear és de 10 i com que no s'aplica cap quota, quan un usuari crea un escriptori, el sistema comprova si el límit establert serà superat. Per tant, no hi ha control sobre la quantitat d'escriptoris que té cada usuari (que seria controlat a través de quotes d'usuari) i s'ofereix una major flexibilitat, ja que es podrien donar moltes combinacions:

* L'usuari 1 podria tenir 6 escriptoris, l'usuari 2 podria tenir 3 escriptoris i l'usuari 3 podria tenir 1 escriptori.
* L'usuari 1 podria tenir 9 escriptoris, l'usuari 2 podria tenir 1 escriptori.
* L'usuari 1 podria tenir 2 escriptoris, l'usuari 2 podria tenir 7 escriptoris i l'usuari 3 podria tenir 1 escriptori.
* L'usuari 1 podria tenir 10 escriptoris, per tant no es podrien crear més escriptoris.
* Etc

## Gestionar Quota

Per a gestionar les quotes i els límits s'ha d'anar al panell d'Administració, es prem el botó ![](./quotas_limits.images/quotas_limits1.png)

![](./quotas_limits.images/quotas_limits2.png)

Premeu el botó ![](./quotas_limits.images/quotas_limits3.png)

![](./quotas_limits.images/quotas_limits4.png)

### Quota d'Usuari

Els paràmetres de la quota d'usuari es poden editar prement la icona ![](./manage_user.images/manage_user13.png) al costat de l'usuari que volem actualitzar, i després prement ![](./quotas_limits.images/quotas_limits9.png).

Apareixerà una finestra de diàleg amb el següent formulari:

![](./quotas_limits.images/quotas_limits10.png)

!!! Info "Notes importants sobre els camps del formulari"
    * Si la casella de selecció "Apply group quota" està seleccionada, els valors del formulari s'ignoraran i s'aplicarà la quota de grup.
    * Quan es desmarqui la casella de selecció "Apply group quota" les caselles d'entrada del formulari s'habilitaran i la quota s'aplicarà a l'usuari de forma **individual**. Consulteu [quotas](#quotas) per veure la finalitat de cada camp.

### Quota de Grup

Els paràmetres de la quota de grup es poden editar prement la icona ![](./manage_user.images/manage_user13.png) al costat del grup que es vol actualitzar, i després prement ![](./quotas_limits.images/quotas_limits9.png).

Apareixerà una finestra de diàleg amb el següent formulari:

![](./quotas_limits.images/quotas_limits11.png)

!!! Info "Notes importants sobre els camps del formulari"
    * Si la casella de selecció "Apply category quota" està seleccionada, els valors del **formulari** **s'ignoraran** i **al grup se li aplicarà la quota de la categoria**.
    * Quan es desmarqui la casella de selecció "Apply category quota" les caselles d'entrada del formulari s'habilitaran. Consulteu [quotes](#quotes) per veure la finalitat de cada camp.
    * La casella de selecció "Override group users current quota" aplicarà la quota del formulari a cada usuari **individualment**. Per tant, se substituirà cadascuna de les quotes dels usuaris pertanyents al grup. Tingueu en compte que els [administradors](../admin/index.md) poden establir quotes més altes per als usuaris que els gestors, per tant, quan se substitueixi la quota d'usuaris, aquestes excepcions també se sobreescriuran.
    * El selector "Apply current changes" aplicarà la quota de formulari **només als usuaris amb el rol seleccionat**.

## Gestionar Límits

### Límits de Grup

Els paràmetres dels límits del grup es poden editar prement la icona ![](./manage_user.images/manage_user13.png) al costat del grup que volem actualitzar, i després prement ![](./quotas_limits.images/quotas_limits12.png).

Apareixerà una finestra de diàleg amb el següent formulari:

![](./quotas_limits.images/quotas_limits13.png)

!!! Info "Notes importants sobre els camps del formulari"
    * Si la casella de selecció "Apply category limits" està seleccionada, els valors del **formulari** **s'ignoraran** i **al grup se li aplicaran els límits de la categoria**.
    * Consulteu els [limits](#limits) per a més informació sobre la funcionalitat dels límits.
