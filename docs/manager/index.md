# Introduction

## Roles

IsardVDI has four different kind of user roles, each one inherits the capabilities from more basic roles. This is the hierarchy from basic user to admin:

- **user**: It is the basic user, commonly assigned to students in schools. This role can create desktops from shared templates with him.
- **advanced**: This role is commonly assigned to teachers in schools. It allows the advanced user to create and share templates from his desktops with other users. Also has the capabilities to create desktop deployments to other users.
- **manager**: Has the capabilities to access web administration interface and manage everything (users, groups, desktops, templates, media) within his category.
- **admin**: It is capable of administering desktop resources, downloads, hypervisors and all system capabilities available.

Here it is a more granular table view of allowed capabilities by item and actions:

## Capabilities

| Item | Action | User Role | Advanced Role | Manager Role | Administrator Role |
| - | - | - | - | - | - |
| Desktop | create        | :material-check: | :material-check: | :material-check: | :material-check: |
| Desktop | edit          | :material-check: | :material-check: | :material-check: | :material-check: |
| Desktop | delete        | :material-check: | :material-check: | :material-check: | :material-check: |
| Desktop | direct viewer | :material-close: | :material-check: | :material-check: | :material-check: |
| | | | | | |
| Template | create        | :material-close: | :material-check: | :material-check: | :material-check: |
| Template | edit          | :material-close: | :material-check: | :material-check: | :material-check: |
| Template | delete        | :material-close: | :material-close: | :material-check: | :material-check: |
| | | | | | |
| Deployment | create        | :material-close: | :material-check: | :material-check: | :material-check: |
| Deployment | edit          | :material-close: | :material-check: | :material-check: | :material-check: |
| Deployment | delete        | :material-close: | :material-check: | :material-check: | :material-check: |
| Deployment | videowall     | :material-close: | :material-check: | :material-check: | :material-check: |

## Capabilities on not owned items

Users from *advanced role* to *administrator role* can also have some actions available on items not owned by them, or to create items to other users.

Legend:

- **deployment**: Role can do the action on desktops belonging to his virtual desktops deployments to other users.
- **category**: Role can do the action on items belonging to his category only.


| Item | Action | User Role | Advanced Role | Manager Role | Admin Role |
| | | | | | |
| Desktop | edit          | :material-close: | deployment | category | :material-check: |
| Desktop | delete        | :material-close: | deployment | category | :material-check: |
| Desktop | direct viewer | :material-close: | deployment | category | :material-check: |
| | | | | | |
| Template | edit          | :material-close: | :material-close: | category | :material-check: |
| Template | delete        | :material-close: | :material-close: | category | :material-check: |
| | | | | | |
| Deployment | edit          | :material-close: | :material-close: | :material-close: | :material-close: |
| Deployment | delete        | :material-close: | :material-close: | category | :material-check: |
| Deployment | videowall     | :material-close: | :material-close: | :material-close: | :material-close: |