# Gestión de usuarios

Para gestionar los usuarios se tiene que ir al panel de Administración, se pulsa el botón ![](./manage_user.es.images/manage_user1.png)

![](./manage_user.es.images/manage_user2.png)

Se pulsa el botón ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Creación de Grupos

Para crear grupos se debe acceder a la subsección "Management" bajo la sección "User" en el panel de "Administración". Seguidamente se debe buscar la tabla "Groups" y pulsar el botón ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

Y se rellenan los campos.


![](./manage_user.images/manage_user3.png)

!!! Info "Notas importantes sobre los campos del formulario"
    * **Linked groups**: Todos los recursos compartidos con sus grupos enlazados se heredarán automáticamente por el grupo creado. Por ejemplo, si un Grupo A se crea con Grupo B como grupo enlazado, todos los recursos compartidos con Grupo B también se compartirán con Grupo A.

    * **Auto desktops creation**: Cuando los usuarios del grupo acceden al sistema se les creará automáticamente un escritorio con la plantilla seleccionada.

    * **Ephimeral desktops**: Establecerá un tiempo limitado de uso a un escritorio temporal. (Para esto también se tiene que configurar, siendo Admin, en Config el "Job Scheduler")


## Edición de Grupos

Se pueden editar los parámetros de un grupo pulsando el icono ![](./manage_user.images/manage_user13.png) junto al grupo que queremos actualizar, y entonces pulsando ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png)

Una ventana de diálogo aparecerá con los mismos parámetros que la ventana de creación.

![](./manage_user.images/manage_user29.png)

## Creación Usuarios

Para crear usuarios hay que acceder a la subsección "Management" bajo la sección "User" en el panel de "Administración".

### Individualmente

Se busca la tabla "Users" y se pulsa el botón ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png)

Y saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user6.png)

Y se rellena los campos.

!!! Info "Notas importantes sobre los campos del formulario"
    * **Secondary groups**: Todos los recursos compartidos con sus grupos secundarios serán heredados automáticamente por el usuario creado. Además, el usuario será añadido a todos los despliegues que sean creados en cualquiera de sus grupos secundarios.

### Creación masiva

Se busca la tabla "Users" y se pulsa el botón ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

Y saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user9.png)

Se puede descargar un archivo de ejemplo pulsando el botón ![](./manage_user.images/manage_user10.png). Saldrá un formulario a rellenar:

!!! Warning "Evitando errores"
    * Los campos "**category**" y "**group**" deben ser **exactamente** iguales a su nombre
    * La codificación recomendada es **Unicode (UTF-8)**
    * Los csv deben de estar separados por **comas** ","
    * Es altamente recomendado utilizar el csv de ejemplo

![](./manage_user.images/manage_user11.png)

Una vez rellenado, se puede subir en 

![](./manage_user.images/manage_user12.png)

Y si se ha subido correctamente, saldrá una tabla de previsualización de los usuarios:

![](./manage_user.images/manage_user28.png)

!!! Failure "Errores"
    Si el csv no se ha subido correctamente se mostrará un error indicando el motivo

## Edición de Usuarios

Para editar usuarios se debe acceder a la subsección "Management" bajo la sección "User" en el panel de "Administración".

### Individualmente

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere actualizar, entonces se pulsa ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png)

Una ventana de diálogo aparecerá con los mismos parámetros que la ventana de creación.

![](./manage_user.images/manage_user35.png)

!!! Info "Notas importantes sobre los campos del formulario"
    * Los campos "**username**", "**category**" y "**group**" no se pueden modificar.

### Edición masiva

Se busca la tabla "Users" y se pulsa el botón ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

Y saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user9.png)

Donde se podrá subir un fichero csv con los datos actualizados. Entonces se selecciona la casilla "Update existing users".


![](./manage_user.images/manage_user36.png)

!!! Warning "Evitando errores"
    * Los campos "**username**", "**category**" i "**group**" tienen que ser **exactamente** iguales a su nombre para actualizar la información del usuario
    * La codificación recomendada es **Unicode (UTF-8)**
    * Los csv deben de estar separados por **comas** ","


!!! Info "Notas importantes sobre los campos del formulario"
    * Los campos "**username**", "**category**" y "**group**" no se pueden modificar. El resto de campos se actualizarán
    * Los usuarios se añadirán a todos los grupos secundarios seleccionados

### Habilitar/Deshabilitar Usuario

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere habilitar/deshabilitar, entonces se pulsa el botón ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png)

El estado de un usuario se puede ver en la tabla de usuarios.

![](./manage_user.images/manage_user38.png)

!!! Danger "Vayan con cuidado"
    * Si el usuario se deshabilita mientras accede al sistema pueden producirse problemas de sesión.

### Cambiar Contrasenya de usuario

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere cambiar la contraseña, entonces se pulsa ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png)

Y saldrá una ventana de diálogo con el siguiente formulario:

![](./manage_user.images/manage_user40.png)

!!! Tip "Sugerencia"
    * El usuario puede cambiar su contraseña mediante su [perfil](../user/profile.es.md).

### Suplantar Usuario

Se pueden editar los parámetros de un usuario pulsando el icono ![](./manage_user.images/manage_user13.png) al lado del usuario que se quiere suplantar, entonces se pulsa ![](./manage_user.images/manage_user44.png).

![](./manage_user.images/manage_user41.png)

!!! Danger "Aviso de exención de responsabilidad"
    Suplantar un usuario **proporciona el acceso a todos sus datos y escritorios y comporta riesgos inherentes**. Antes de continuar, es importante **considerar la sensibilidad de la información a la cual se accederá**.

### Restaurar VPN

El botón "Reset VPN" permite regenerar nuevas claves VPN para el usuario. Al pulsar este botón, las claves VPN existentes asociadas se reemplazarán por otras nuevas, lo que implica que el usuario tendrá que descargar un nuevo archivo VPN y configurarlo para seguir usando la conexión.

![](./manage_user.images/manage_user45.png)


## Clave de autoregistro

En la tabla "Grupos" se busca el nombre del grupo del que se quiera obtener el codigo. Se pulsa el botón ![](./manage_user.images/manage_user13.png)

![](./manage_user.images/manage_user14.png)

El grupo se despliega con unas opciones. Se pulsa el botón ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

Se abrirá una ventana de diálogo donde se pueden generar códigos pulsando las diferentes casillas de selección.

![](./manage_user.images/manage_user32.png)

Una vez generado el código de autoregistro este puede ser copiado y compartido con los usuarios.

![](./manage_user.images/manage_user17.png)

!!! Danger "Vayan con cuidado"
    * Al registrarse, **los usuarios tendrán el rol del código de autoregistro compartido**. Por ejemplo, a los profesores, se les dará el código "Advanced"y a los alumnos el código de "Users".
    * La cantidad de usuarios que pueden registrarse con un código es ilimitado. Por este motivo, es recomendable desactivarlos una vez utilizados.
