# Paperera de reciclatge

Per a visualitzar els elements a la paperera de reciclatge s'ha d'anar al panell d'Administració, es prem el botó ![](./manage_user.ca.images/manage_user1.png)

![](./manage_user.ca.images/manage_user2.png){width="90%"}

Es prem el botó ![](./recycle_bin.ca.images/1.png)

![](./recycle_bin.ca.images/2.png){width="40%"}

A l'apartat de paperera de reciclatge es podran visualitzar els diferents elements esborrats provisionalment.

![](./recycle_bin.ca.images/3.png)

### Veure informació dels elements esborrats

Es pot accedir fent clic a la icona ![](./recycle_bin.ca.images/4.png) de la fila corresponent. Es podrà accedir a la informació detallada dels elements esborrats i les seves dependències:

![](./recycle_bin.ca.images/5.png)

### Restaurar elements esborrats

Es poden recuperar els elements esborrats fent clic a la icona ![](./recycle_bin.ca.images/6.png).

### Eliminar definitivament

Es poden eliminar definitivament els elements esborrats fent clic a la icona ![](./recycle_bin.ca.images/7.png).

### Modificar el temps d'esborrat automàtic

Es pot modificar el temps màxim que pot estar un element a la paperera de reciclatge mitjançant el desplegable situat a la part superior:

![](./recycle_bin.ca.images/8.png)

### Enviar elements a la paperera per defecte

!!! info "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

Es pot configurar el sistema perquè enviï els elements esborrats a la paperera de reciclatge per defecte.

Per a anar a la secció de configuració de la paperera de reciclatge, es prem el botó sota l'apartat "Recycle bin"

![](./recycle_bin.ca.images/10.png)

On podrem marcar la casella "Send to recycle bin by default", que marcarà per defecte la casella d'enviar a la paperera quan un usuari esborri un element:

![](./recycle_bin.ca.images/11.png)

![](./recycle_bin.ca.images/12.png)