# Cuotas y Límites

## Cuotas

Las cuotas definen la cantidad de recursos que se pueden utilizar  **individualmente** (por cada usuario), en función de la categoría y el grupo al cual pertenece el usuario.

Las restricciones de cuotas se dividen de la manera siguiente:

![](./quotas_limits.images/quotas_limits8.png)

**Cuota de creación**

* **Desktops**: Cantidad de escritorios que el usuario puede crear.
* **Templates**: Cantidad de plantillas que el usuario puede crear.
* **Media**: Cantidad de medios que el usuario puede subir o descargar.

!!! Warning "Aviso"
    * Los **recursos compartidos** con el usuario **no se consideran en la cuota de usuario**.
    * Los escritorios creados **a través de despliegues no se consideran en la cuota de usuario**.

**Cuota de escritorios en ejecución**

* **Concurrent**: Cantidad total de escritorios que se pueden iniciar a la vez.
* **vCPUs**: Cantidad total de vCPUS de los escritorios iniciados que se pueden utilizar a la vez.
* **Memory (GB)**: Cantidad total de memoria vRAM de los escritorios iniciados que se pueden utilizar a la vez.

!!! Warning "Aviso"
    * Cuando se inicia un **escritorio desde un despliegue, se considera la cuota del usuario**. Por lo tanto, si el usuario no tiene suficiente cuota para iniciar el escritorio, se mostrará un error, incluso si el propietario del despliegue tiene suficiente cuota. La lógica detrás de este comportamiento es la propiedad del escritorio, el propietario del escritorio es el usuario, a pesar de que fue creado por el propietario del despliegue.

**Cuota de tamaño**

* **Disk size**: Medida máxima permitida al crear un escritorio desde un medio.
* **Total size**: Medida total que se puede utilizar considerando escritorios, plantillas y medios.
* **Soft size**: Una vez ocupada esta cantidad, se avisará al usuario.

Las cuotas se pueden establecer por usuario, grupo o categoría. Se aplican por niveles, empezando de bajo arriba (se aplicará la cuota de nivel inferior). Por ejemplo, considerando la estructura siguiente:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Category --> Group2[Group 2]
    Category --> Group3[Group 3]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
    Group2 --> User4[User 4]
    Group2 --> User5[User 5]
    Group2 --> User6[User 6]
    Group3 --> User7[User 7]
    Group3 --> User8[User 8]
    Group3 --> User9[User 9]
```

* Si el usuario tiene una **cuota de usuario establecida** esta será aplicada. Las cuotas de **grupo y categoría** serán **ignoradas**.
* Si el usuario **no tiene una cuota de usuario establecida** y se ha establecido una **cuota de grupo**, se aplicará la cuota de grupo. La **cuota de categoría se ignorará**.
* Si no han sido establecidas **ni la cuota de usuario ni la de grupo se aplicará la cuota de categoría**. La cuota de categoría solo se puede establecer con un [rol de administrador](../admin/index.md).
* Si **no se aplica ninguna cuota**, todos los usuarios tendrán acceso a crear escritorios, plantillas y isos **ilimitadas** con vCPU, vRAM de memoria y medida de disco **ilimitadas**.

!!! Tip "Suggeriment"

    La restricción aplicada (cuota de usuario, grupo o de categoría) y el uso de los recursos se pueden consultar a través de la [página de perfil](../user/profile.md).

    === "Cuota de usuario aplicada"
        ![](./quotas_limits.images/quotas_limits5.png)

    === "Cuota de grupo aplicada"
        ![](./quotas_limits.images/quotas_limits6.png)

    === "Cuota de categoría aplicada"
        ![](./quotas_limits.images/quotas_limits7.png)

## Límites

Los límites definen la cantidad de recursos que se pueden utilizar globalmente en una categoría o un grupo.

Por ejemplo, considerando un Grupo 1 con un límite de 10 escritorios sin ninguna cuota aplicada:

``` mermaid
flowchart TD
    Category --> Group1[Group 1]
    Group1 --> User1[User 1]
    Group1 --> User2[User 2]
    Group1 --> User3[User 3]
```

La cantidad total de escritorios que se pueden crear es de 10 y como que no se aplica ninguna cuota, cuando un usuario crea un escritorio, el sistema comprueba si el límite establecido será superado. Por lo tanto, no hay control sobre la cantidad de escritorios que tiene cada usuario concretamente (que sería controlado a través de cuotas de usuario) y se ofrece una mayor flexibilidad, puesto que se podrían dar muchas combinaciones:

* El usuario 1 podría tener 6 escritorios, el usuario 2 podría tener 3 escritorios y el usuario 3 podría tener 1 escritorio.
* El usuario 1 podría tener 9 escritorios, el usuario 2 podría tener 1 escritorio.
* El usuario 1 podría tener 2 escritorios, el usuario 2 podría tener 7 escritorios y el usuario 3 podría tener 1 escritorio.
* El usuario 1 podría tener 10 escritorios, por lo tanto no se podrían crear más escritorios.
* Etc

## Gestionar Cuota

Para gestionar las cuotas y los límites se tiene que ir al panel de Administración, se pulsa el botón ![](./quotas_limits.images/quotas_limits1.png)

![](./quotas_limits.images/quotas_limits2.png)

Se pulsa el botón ![](./quotas_limits.images/quotas_limits3.png)

![](./quotas_limits.images/quotas_limits4.png)

### Cuota de Usuario

Los parámetros de la cuota de usuario se pueden editar pulsando el icono ![](./manage_user.images/manage_user13.png) junto al usuario que queremos actualizar, y después pulsando ![](./quotas_limits.images/quotas_limits9.png).

Aparecerá una ventana de diálogo con el siguiente formulario:

![](./quotas_limits.images/quotas_limits10.png)

!!! Info "Notas importantes sobre los campos del formulario"
    * Si la casilla de selección "Apply group quota" está seleccionada, los valores del formulario se ignorarán y se aplicará la cuota de grupo.
    * Cuando se desmarque la casilla de selección "Apply group quota" las casillas de entrada del formulario se habilitarán y la cuota se aplicará al usuario de forma **individual**. Consultad [cuotas](#quotas) para ver la finalidad de cada campo.

### Cuota de Grupo

Los parámetros de la cuota de grupo se pueden editar pulsando el icono ![](./manage_user.images/manage_user13.png) junto al grupo que queremos actualizar, y después pulsando ![](./quotas_limits.images/quotas_limits9.png).

Aparecerá una ventana de diálogo con el siguiente formulario:

![](./quotas_limits.images/quotas_limits11.png)

!!! Info "Notas importantes sobre los campos del formulario"
    * Si la casilla de selección "Apply category quota" está seleccionada, los valores del **formulario** **se ignorarán** y **al grupo se le aplicará la quota de la categoría**.
    * Cuando se desmarque la casilla de selección "Apply category quota" las casillas de entrada del formulario se habilitarán. Consultad [cuotas](#quotes) para ver la finalidad de cada campo.
    * La casilla de selección "Override group users current quota" aplicará la cuota del formulario a cada usuario **individualmente**. Por lo tanto, se sustituirá cada una de las cuotas de los usuarios pertenecientes al grupo. Tened en cuenta que los [administradores](../admin/index.md) pueden establecer cuotas más altas para los usuarios que los gestores, por lo tanto, cuando se sustituya la cuota de usuarios, estas excepciones también se sobreescribirán.
    * El selector "Apply current changes" aplicará la cuota de formulario **solo a los usuarios con el rol seleccionado**.

## Gestionar Límites

### Límites de Grupo

Los parámetros de los límites del grupo se pueden editar pulsando el icono ![](./manage_user.images/manage_user13.png) junto al grupo que queremos actualizar, y después pulsando ![](./quotas_limits.images/quotas_limits12.png).

Aparecerá una ventana de diálogo con el siguiente formulario:

![](./quotas_limits.images/quotas_limits13.png)

!!! Info "Notas importantes sobre los campos del formulario"
    * Si se selecciona la casilla "Apply category limits" los valores del **formulario** **se ignorarán** y **al grupo se le aplicarán los límites de la categoría**.
    * Consultad los [límites](#limits) para más información sobre la funcionalidad de los límites.
