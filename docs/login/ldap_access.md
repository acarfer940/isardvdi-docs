# LDAP Access

If your organization has a LDAP server and the IsardVDI administrator has configured it, you can login using those credentials. The process is very similar to Local Access. First you have to access https://DOMAIN, then select category and language: 

![](./local_access.images/local_access1.png)

If the category dropdown doesn't exist don't worry, IsardVDI might have autologin enabled or you might have accesed the category directly:

![](./local_access.images/local_access2.png)

The choice of language is saved in a cookie in the browser, and the next time you enter it will appear preselected with the language you chose the last time.

## Registration by registration code

If this is the first time you are accessing as a user and your administrator hasn't enabled autologin, a form will appear to enter a registration code.

The registration code is created by the organization manager and how it is generated is explained later in the "Manager" section of the documentation. Each user must belong to a group and have a role, either advanced (teacher), user (student), etc. For each group of users, self-registration codes can be generated for different roles.

Enter the code provided by a Manager and press the button ![](./oauth_access.images/code2.png)

![](./oauth_access.images/code1.png)

And you access the basic interface authenticated with LDAP!

![](./oauth_access.images/home1.png)

