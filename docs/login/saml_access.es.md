# Acceso SAML

Se pulsa el botón ![](saml_access.images/button.png){width="5%"} de la página principal de IsardVDI.

![](local_access.es.images/local_access1.png){width="80%"}

A partir de aquí, se continua con el proceso de inicio de sesión conocido por el usuario, que es distinto según cada servidor SAML.


## Inscripción por código de registro

Se introduce el código proporcionado por un administrador o gesto de la plataforma y se pulsa el botón ![](oauth_access.ca.images/codi2.png){width="30%"}

![](./oauth_access.es.images/codigo1.png){width="30%"}

Y se accede a la página principal

![](./oauth_access.es.images/home1.png){width="80%"}

!!! note "Nota"
    Una vez autenticados, ya **no** hace falta repetir el proceso de registro. Cuando se quiera volver a acceder mediante SAML, sólo se tiene que pulsar en el botón ![](./saml_access.images/button.png){width="5%"} y utilizar las **credenciales de SAML**.