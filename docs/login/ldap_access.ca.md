# Accés LDAP

Si la teva organització té un servidor LDAP i l'administrador d'IsardVDI l'ha configurat, pots accedir utilitzant aquelles credencials. El procés és molt similar a l'accés Local. Primer has d'accedir a https://DOMINI, i seleccionar la categoria i l'idioma:

![](./local_access.ca.images/local_access1.png)

Si no apareix el seleccionador de categoria, no et preocupis. Potser hi ha l'accés automàtic configurat, o bé has accedit a través de la URL de la categoria directament:

![](./local_access.ca.images/local_access2.png)

L'elecció de l'idioma es queda guardada en un cookie en el navegador, i la pròxima vegada que entris ja apareixerà preseleccionada amb l'idioma que vas triar l'última vegada.

## Inscripció per codi de registre

Si és la primera vegada que s'accedeix com a usuari apareixerà el formulari per a introduir un codi de registre. 

El codi de registre es crea per part del manager de l'organització i s'explica com es genera més endavant en la secció de "Manager" de la documentació. Cada usuari ha de pertànyer a un grup i tenir un rol, ja sigui advanced (professor), user (alumne), etc. Per cada grup d'usuaris es poden generar codis d'autoregistre per a diferents rols.

S'introdueix el codi proporcionat per un Manager i es prem el botó ![](./oauth_access.ca.images/codi2.png)

![](./oauth_access.ca.images/codi1.png)

I s'accedeix a la interfície bàsica autenticats amb el compte d'LDAP!

![](./oauth_access.ca.images/home1.png)
