# Introducció

Per accedir al servidor s'ha d'accedir a l'enllaç **https://domini_isardvdi** on es mostrarà la pàgina d'inici de sessió.

![](local_access.ca.images/local_access1.png){width="80%"}

!!! info "El compte d'usuari pot ser d'un d'aquest tipus:"
    - **[Local](../local_access.ca)**: S'han proporcionat la **categoria** i **credencials**.
    - **[OAuth2 (Google/Gitlab)](../oauth_access.ca)**: S'ha proporcionat un **codi d'accés** necessari per a completar el registre junt amb el compte personal.
    - **[SAML](../saml_access.ca)**: Credencials corporatives per a iniciar la sessió. També s'ha proporcionat un **codi d'accés** necessari per a completar l'autoregistre.
