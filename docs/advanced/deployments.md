# Deployments

!!! Info
    This section is only for **administrators**, **managers** and **advanced users**

Deployments are a powerful feature that allows advanced users to create a set of desktops based on a single template. Every user included in a deployment will have access to their own unique desktop.

With deployments, you can create and manage multiple desktops simultaneously for all chosen users. As the creator, you can easily manage all the desktops in one place through the Deployments section. You even have the ability to interact with each desktop and view the screens simultaneously.

## How to create deployments

To create a deployment, you must go to the top bar and press the button ![Deployments button topbar](./deployments.images/deployments1.png)

It will redirect to this page:

![deployments page](./deployments.images/deployments4.png)

Press the button ![New Deployment](./deployments.images/deployments3.png)

You will be directed to a form where you can enter the necessary information for your deployment, including the name of the deployment, each desktop name that users will see, and a brief description.

![new deployment form](./deployments.images/deployments6.png)

Additionally, you must select a template from which to create your desktops. Once created, your desktops will be made visible to users, click on ![not visible / visible](./deployments.images/deployments5.png)

Furthermore, you are required to specify the users that should be included in the deployment. In the final section of the form, you can select the necessary group of users as well as any additional users.

!!! Note
    A desktop for your user will also be created inside the deployment

The last section is the advanced options. Here, you can change the viewers of the desktops, the hardware and the thumbnail.

### Start and Stop

To access the deployment, you need to click on the deployment name.

![deployment list](./deployments.images/deployments7.png)

To start a desktop, press the button ![Start](./deployments.images/deployments8.png)

Once started, it will detect the "IP" of the desktop (if it has one), the "State" (If it is started or stopped), and the type of viewer can be selected

You can select the viewer you are interested in by pressing the drop-down menu

![viewers dropdown](./deployments.images/deployments11.png)

To stop a started desktop, simply to click on the ![stop](./deployments.images/deployments13.png) button that will appear. This will change its status to "Shutting Down". While you can use the "Force Stop" option for an immediate stop, it may lead to closure issues and potential problems.

To stop all desktops, press the button ![stop icon](./deployments.ca.images/deployments16.png)

## Videowall

Videowall is a feature that allows you to view all the screens desktops of a particular deployment simultaneously and in real-time. It also gives you the ability you to interact with any of the displayed desktops and use them as your own.

!!! Warning
    Entering another user's screen will take the control of the owner user.
  
!!! Danger "Disclaimer"
    Please be aware that the owner's permission is essential before entering their desktop due to privacy concerns.

To access Videowall, click on the button ![videowall icon](./deployments.ca.images/deployments18.png)

You will be redirected to a page where you can see all the users' desktops.

![videowall screen](./deployments.images/deployments17.png)

To interact with a desktop, click on button ![maximize](./deployments.ca.images/deployments21.png) which will redirect you to a full-screen view of the selected desktop in full screen.

![full screen videowall](./deployments.images/deployments18.png)

!!! Warning "RDP Viewers"
    Videowall won't display desktops that use RDP viewers

## Make visible/invisible

To make the deployment visible or invisible means that users within the deployment can either view and access their desktop or not.

### Visible

To make visible, click the button ![eye crossed button](./deployments.ca.images/deployments23.png)

A dialog box will appear

![make visible modal](./deployments.images/deployments20.png)

Once "Visible" is selected, the deployment will turn green

![green deployment](./deployments.images/deployments21.png)

### Invisible

To make invisible, click the button ![eye open button](./deployments.ca.images/deployments26.png)

A dialog box will appear

![make invisible modal](./deployments.images/deployments23.png)

There are two options to choose from:

- **Don't stop desktops**: This option will make the deployment invisible to the user. However, if a user is currently using their desktop at the time it's made invisible, they will still be able to use it until they decide it's stopped. Once it's stopped, the user won't be able to access it again until the deployment is made visible again.

- **Stop desktops**: This option will also make the deployment invisible. However, if any user is already using their desktop, it will shut down their desktop immediately.

Once "Invisible" is selected, the deployment will turn red

![red deployment](./deployments.images/deployments24.png)

## Recreate desktops

The "Recreate Desktops" function is intended to modify the deployment by adding or removing desktops based on changes made to user permissions. If a user is added or removed, this function will update the deployment accordingly by adding or removing their respective desktops.

To do so, click the button ![recreate icon](./deployments.ca.images/deployments31.png)

![notification](./deployments.images/deployments26.png)

## Direct viewer file

This file is used to view the list of users in the deployment and their respective desktop URLs. Follow the steps below to download this file:

First click the button ![download icon](./deployments.ca.images/deployments34.png)

A notification will appear:

![notification](./deployments.images/deployments27.png)

You must confirm the notification to download the CSV file.

This file contains every username, their name, their email and a link that redirects to the user's desktop.

![csv file content](./deployments.ca.images/deployments36.png)

## Delete

To delete a deployment, press the button ![trash icon](./deployments.ca.images/deployments37.png) in the deployment list. All the desktops inside will be removed too.
