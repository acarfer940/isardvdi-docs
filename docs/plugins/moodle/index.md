# Moodle plugin

This plugin will allow to jump from moodle activity to IsardVDI. User account will be created from Moodle in IsardVDI if doesn’t exist.

As a teacher an IsardVDI activity can be added to a course. The users enrolled are then able to jump to it’s own virtual desktops in IsardVDI from Moodle from this activity with SSO. The moodle user groups will be created in IsardVDI from course names and users will also be created if don’t exist yet.

The moodle plugin src it is located at [https://gitlab.com/isard/plugin-moodle/](https://gitlab.com/isard/plugin-moodle/). Check in the README how to install and details about this plugin.

