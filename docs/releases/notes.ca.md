# Llançaments

El flux de desenvolupament de l'IsardVDI és *rolling release*, publicacions contínues.

En un model de desenvolupament de publicacions contínues, el desenvolupament de programari està en curs i les noves actualitzacions es publiquen contínuament tan aviat com estiguin preparades. En lloc d'esperar una data de llançament específica per introduir noves característiques i millores, les actualitzacions es publiquen tan aviat com es desenvolupin i es posin a prova, permetent als usuaris estar actualitzats amb l'última versió del programari en tot moment.

En un model de desenvolupament de publicacions contínues, no hi ha números de versió específics o llançaments importants com en un model de desenvolupament de programari tradicional. En canvi, les noves actualitzacions s'identifiquen per la data o el número de construcció, i els usuaris poden actualitzar a la darrera versió en qualsevol moment.

Aquest enfoc permet actualitzacions més freqüents i un lliurament més ràpid de noves característiques i correccions d'errors, però també requereix més proves i control de qualitat per assegurar que les noves actualitzacions no introdueixin problemes nous o trenquin la funcionalitat existent. També requereix que els usuaris estiguin informats sobre mantenir el seu programari actualitzat, ja que les noves actualitzacions poden requerir configuració addicional o ajustos per funcionar correctament.

Per tant, ara podeu seguir les actualitzacions noves a la branca principal a [https://gitlab.com/isard/isardvdi/-/commits/main](https://gitlab.com/isard/isardvdi/-/releases)